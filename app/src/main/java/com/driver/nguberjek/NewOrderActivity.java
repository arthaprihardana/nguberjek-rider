package com.driver.nguberjek;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.NguberjekRiderDialog;
import com.driver.nguberjek.model.OrderDetailModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NewOrderActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = NewOrderActivity.class.getSimpleName();

    AlertDialog globalDialog;

    ProgressBar mProgressBar;
    CountDownTimer takeOrderTimer;

    OrderDetailModel orderDetailModel;

    LinearLayout dataOrder, loading;
    Button btnTolak, btnTerima;
    ImageView ivGifWaiting;

    int start = 0, max = 30;
    String pushNotif = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_order);

        setVisibilty();

        setModel();
    }

    public void setVisibilty(){
        loading = (LinearLayout) findViewById(R.id.loading);
        dataOrder = (LinearLayout) findViewById(R.id.data_order);
        ivGifWaiting = (ImageView) findViewById(R.id.gif_waiting);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(ivGifWaiting);
        Glide.with(this).load(R.drawable.waiting_response).into(imageViewTarget);

        loading.setVisibility(View.GONE);
        dataOrder.setVisibility(View.VISIBLE);
        ivGifWaiting.setVisibility(View.GONE);

    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        AppConstans.orderID = b.getString("orderId");
        pushNotif = b.getString("pushNotif");

        if (pushNotif.equals("0")) {
            checkOrder();
        } else if (pushNotif.equals("8")) {
            Toast.makeText(NewOrderActivity.this, "Maaf, penumpang telah lebih dulu diterima oleh rider lain", Toast.LENGTH_LONG).show();
            listOrder();
        } else {
            Toast.makeText(NewOrderActivity.this, "Maaf, order telah dibatalkan oleh boncenger", Toast.LENGTH_LONG).show();
            listOrder();
        }
    }

    public void checkOrder() {
        String url = AppConstans.APIOrderDetail + AppConstans.orderID;
        String tag_json_obj = "tempOrder";

        setDialog2();
        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    if (status == 1) {
                        hideDialog2();
                        dataOrder.setVisibility(View.VISIBLE);

                        JSONObject data = response.getJSONObject("dt");

                        orderDetailModel = new OrderDetailModel(data);

                        setContent();
                    } else {

                        Toast.makeText(NewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    Log.d(TAG, e.getMessage());

                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    checkOrder();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrderActivity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void setContent() {
        TextView tvNama = (TextView) findViewById(R.id.tv_nama);
        TextView tvDari = (TextView) findViewById(R.id.tv_dari);
        TextView tvKe = (TextView) findViewById(R.id.tv_ke);
        TextView tvHarga = (TextView) findViewById(R.id.tv_harga);

        btnTerima = (Button) findViewById(R.id.btn_terima);
        btnTolak = (Button) findViewById(R.id.btn_tolak);

        tvNama.setText(orderDetailModel.nama);
        tvDari.setText(orderDetailModel.lokasiAwal);
        tvKe.setText(orderDetailModel.lokasiTujuan);
        tvHarga.setText("Rp. " + rupiahFormat.format(Double.parseDouble(orderDetailModel.harga)) + ";");

        dataOrder.setVisibility(View.VISIBLE);
        ivGifWaiting.setVisibility(View.GONE);
        setProgress();

        btnTerima.setOnClickListener(this);
        btnTolak.setOnClickListener(this);
    }

    public void setProgress() {
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);

        mProgressBar.setMax(max);
        mProgressBar.setProgress(start);

        takeOrderTimer = new CountDownTimer(300000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress" + start + millisUntilFinished);
                start++;
                mProgressBar.setProgress(start);

                max--;
            }

            @Override
            public void onFinish() {
                rejectOrder();
            }
        };

        takeOrderTimer.start();
    }

    @Override
    public void onClick(View v) {
        if (v == btnTolak) {
            if (takeOrderTimer != null) {
                takeOrderTimer.cancel();
            }

            rejectOrder();
        } else if (v == btnTerima) {
            if (takeOrderTimer != null) {
                takeOrderTimer.cancel();
            }

            acceptOrderDetail();
        }
    }

    public void acceptOrderDetail() {
        String url = AppConstans.APIAcceptOrder;
        String tag_json_obj = "accept_order";

        setDialog3();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    if (status == 1) {

                    } else {
                        ivGifWaiting.setVisibility(View.GONE);
                        dataOrder.setVisibility(View.VISIBLE);

                        Toast.makeText(NewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                        /*listOrder();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    Log.d(TAG, e.getMessage());

                    ivGifWaiting.setVisibility(View.GONE);
                    dataOrder.setVisibility(View.VISIBLE);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    acceptOrderDetail();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrderActivity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                ivGifWaiting.setVisibility(View.GONE);
                dataOrder.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("orderId", orderDetailModel.orderId);
                params.put("riderId", user.get(SessionManager.KEY_ID));

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void rejectOrder() {
        String url = AppConstans.APIRejectORder;
        String tag_json_obj = "rejectOrder";

        setDialog2();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    if (status == 1) {
                        hideDialog2();

                        Toast.makeText(NewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                        AppConstans.orderID = "";
                        listOrder();
                        finish();
                    } else {
                        hideDialog2();
                        dataOrder.setVisibility(View.VISIBLE);

                        Toast.makeText(NewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog2();
                    dataOrder.setVisibility(View.VISIBLE);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    rejectOrder();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrderActivity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                hideDialog2();
                dataOrder.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("riderId", user.get(SessionManager.KEY_ID));
                params.put("orderId", AppConstans.orderID);

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void listOrder() {
        String url = AppConstans.APIListOrder + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "listOrder";

        setDialog2();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    if (status == 1) {
                        JSONArray data = response.getJSONArray("dt");

                        if (data.length() != 0) {
                            Intent i = new Intent(NewOrderActivity.this, NewOrderActivity.class);
                            Bundle b = new Bundle();
                            b.putString("orderId", data.getJSONObject(0).getString("orderId"));
                            b.putString("pushNotif", "0");
                            i.putExtras(b);
                            startActivity(i);
                        }
                        finish();

                    } else {
                        Toast.makeText(NewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    finish();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    listOrder();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrderActivity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (takeOrderTimer != null) {
            takeOrderTimer.cancel();
        }

        if (globalDialog != null && globalDialog.isShowing()) {
            globalDialog.dismiss();
        }
    }

    public void setDialog2(){
        loading.setVisibility(View.VISIBLE);
        dataOrder.setVisibility(View.INVISIBLE);
        ivGifWaiting.setVisibility(View.GONE);
    }

    public void setDialog3(){
        loading.setVisibility(View.GONE);
        dataOrder.setVisibility(View.GONE);
        ivGifWaiting.setVisibility(View.VISIBLE);
    }


    public void hideDialog2(){
        loading.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {

    }
}
