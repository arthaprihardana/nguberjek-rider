package com.driver.nguberjek;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.driver.nguberjek.utilities.BaseActivity;

public class SelfieOrderActivity extends BaseActivity implements View.OnClickListener {

    Button btnOk;
    EditText etNama, etNomorTelepon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfie_order);

        setActionBar();
        setContent();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Selfie Order");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setContent() {
        btnOk = (Button) findViewById(R.id.btn_ok);
        etNama = (EditText) findViewById(R.id.et_nama);
        etNomorTelepon = (EditText) findViewById(R.id.et_nomorTelepon);

        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnOk) {
            if (etNama.getText().toString().length() != 0 && etNomorTelepon.getText().toString().length() != 0) {
                String[] dataPelanggan = {etNama.getText().toString(), etNomorTelepon.getText().toString()};

                Intent intent = new Intent(SelfieOrderActivity.this, MapSelfieOrderActivity.class);
                Bundle b = new Bundle();
                b.putStringArray("dataPelanggan", dataPelanggan);
                intent.putExtras(b);
                startActivity(intent);
            }else{
                if(etNama.getText().toString().length() == 0)
                    etNama.setError("This field is required");
                else etNomorTelepon.setError("This field is required");
            }
        }
    }
}
