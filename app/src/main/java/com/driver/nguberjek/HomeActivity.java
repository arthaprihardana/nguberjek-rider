package com.driver.nguberjek;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.bumptech.glide.Glide;
import com.driver.nguberjek.app.SharedPreference;
import com.driver.nguberjek.model.Session;
import com.driver.nguberjek.pushnotification.AudioPlayer;
import com.driver.nguberjek.pushnotification.RegistrationIntentService;
import com.driver.nguberjek.pushnotification.SoundPoolPlayer;
import com.driver.nguberjek.utilities.ConvertRupiah;
import com.driver.nguberjek.utilities.SendRiderCoordinate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.driver.nguberjek.adapter.NavigationDrawerAdapter;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.InformasiDialog;
import com.driver.nguberjek.model.NavigationDrawerModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnMapReadyCallback, LocationListener {
    private final String TAG = HomeActivity.class.getSimpleName();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    GoogleMap map;
    UiSettings mUiSettings;
    LocationManager locationManager;
    String locationProvider;

    Bundle b;

    CoordinatorLayout coordinatorLayout;
    ImageView ivEditProfile, ivLocation;
    TextView tvSelfieOrder, tvSaldo;
    Button btn_test;
    Switch sw_ready;
    TextView tv_switch;
    RelativeLayout container_switch_info;

    boolean statusInformasiDialog = true;
    int icon = 0;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (checkPlayServices()) {
            Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
            startService(intent);
        }



        setContent();
        setNavigation();
        menuNavigationDrawer();

        //setMap();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);





        // SEND MY COORDINATE //

        sendRiderCoordinate = new SendRiderCoordinate(mActivity, user);

    }

    private void setNavigation(){
        btn_test=(Button)findViewById(R.id.btn_test);
        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user.get(SessionManager.KEY_READY).equals("true")){
                    updateDataRider(false);
                }else{
                    updateDataRider(true);
                }
            }
        });
        container_switch_info=(RelativeLayout)findViewById(R.id.switch_info);
        sw_ready=(Switch)findViewById(R.id.switch_ready);
        tv_switch=(TextView)findViewById(R.id.tv_switch);
        if(user.get(SessionManager.KEY_READY).equals("true")){
            sw_ready.setChecked(true);
            container_switch_info.setVisibility(View.GONE);
            tv_switch.setText("ON");
        }else {
            sw_ready.setChecked(false);
            container_switch_info.setVisibility(View.VISIBLE);
            tv_switch.setText("OFF");
        }
        sw_ready.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateDataRider(isChecked);
            }
        });
    }




    public void updateDataRider(final boolean is_ready) {
        String url = AppConstans.APIUpdateDataRider + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "change_availabe_rider";

        setDialog();
        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    hideDialog();
                    if (status == 1) {
                        //Toast.makeText(HomeActivity.this, message.toLowerCase(), Toast.LENGTH_SHORT).show();
                        login(user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD));
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(HomeActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    updateDataRider(is_ready);
                    loginAPI.loginAPI = false;
                }else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                logManager.logD(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("ready",is_ready+"");
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    public void setNavigationDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        ImageView ivStar1 = (ImageView) headerView.findViewById(R.id.iv_star1);
        ImageView ivStar2 = (ImageView) headerView.findViewById(R.id.iv_star2);
        ImageView ivStar3 = (ImageView) headerView.findViewById(R.id.iv_star3);
        ImageView ivStar4 = (ImageView) headerView.findViewById(R.id.iv_star4);
        ImageView ivStar5 = (ImageView) headerView.findViewById(R.id.iv_star5);
        ImageView ivProfilePicture = (ImageView) headerView.findViewById(R.id.iv_profilPicture);
        ivEditProfile = (ImageView) headerView.findViewById(R.id.iv_editProfil);
        TextView tvNamaLengkap = (TextView) headerView.findViewById(R.id.tv_namaLengkap);
        TextView tvNomorTelepon = (TextView) headerView.findViewById(R.id.tv_nomorTelepon);
        TextView tvPlat = (TextView) headerView.findViewById(R.id.tv_plat);

        if (user.get(SessionManager.KEY_RATING).trim().equals("1")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star_line);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("2")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("3")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("4")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("5")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star);
            ivStar5.setImageResource(R.mipmap.star);
        } else {
            ivStar1.setImageResource(R.mipmap.star_line);
            ivStar2.setImageResource(R.mipmap.star_line);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        }
        user = sessionManager.getUserDetails();
        tvNamaLengkap.setText(user.get(SessionManager.KEY_NAMA));
        tvNomorTelepon.setText(user.get(SessionManager.KEY_NO_TELP));
        tvPlat.setText(user.get(SessionManager.KEY_NO_POLISI));

        logManager.logD(TAG, user.get(SessionManager.KEY_PHOTO));

        Glide.with(this)
                .load(user.get(SessionManager.KEY_PHOTO))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .override(150,150)
                .into(ivProfilePicture);

        ivEditProfile.setOnClickListener(this);
    }

    public void setContent() {
        AppConstans.orderID = "";

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        tvSelfieOrder = (TextView) findViewById(R.id.tv_selfieOrder);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        ivLocation = (ImageView) findViewById(R.id.iv_location);

        tvSelfieOrder.setOnClickListener(this);
        ivLocation.setOnClickListener(this);
    }

    public void menuNavigationDrawer() {
        RecyclerView mRecyclerView, mRecyclerView2;

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_menuNd);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<NavigationDrawerModel> list = new ArrayList<NavigationDrawerModel>();
        NavigationDrawerModel navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(1);

        list.add(navigationDrawerModel);

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(2);

        list.add(navigationDrawerModel);

        /*navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(3);

        list.add(navigationDrawerModel);*/

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(4);

        list.add(navigationDrawerModel);

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(5);

        list.add(navigationDrawerModel);

        NavigationDrawerAdapter navDrawAdapter = new NavigationDrawerAdapter(HomeActivity.this, list);
        mRecyclerView.setAdapter(navDrawAdapter);

        mRecyclerView2 = (RecyclerView) findViewById(R.id.rv_menuNd2);
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<NavigationDrawerModel>();

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(6);

        list.add(navigationDrawerModel);

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(7);

        list.add(navigationDrawerModel);


        NavigationDrawerAdapter navDrawAdapter2 = new NavigationDrawerAdapter(HomeActivity.this, list);
        mRecyclerView2.setAdapter(navDrawAdapter2);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (isMapPermissionGranted()) setMap();

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);
        if (this.map != null) {
            this.initializeMap();
        }
        //initialize the locaiton manager
        this.initializeLocationManager();
        this.locationManager.requestLocationUpdates(this.locationProvider, 400, 1, this);

        setCoordinate();
        setMap();
    }

    public void setMap() {
        //map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 14));
        map.animateCamera(CameraUpdateFactory.zoomTo(14), 5000, null);

        if (user.get(SessionManager.KEY_ROLE).equals("nguberjek"))
            icon = R.mipmap.rider;
        else if (user.get(SessionManager.KEY_ROLE).equals("ngubertaxi"))
            icon = R.mipmap.driver_taxi;
        else if (user.get(SessionManager.KEY_ROLE).equals("ngubercar"))
            icon = R.mipmap.driver_car;
        else icon = R.mipmap.rider;

        map.clear();
        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(icon))
                .position(getCurrentLocation()));

    }


    public boolean isMapPermissionGranted(){
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            setMap();
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    public void perjalananTerakhir() {
        String url = AppConstans.APILastOrder + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "lastorder";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        int statusCode = response.getInt("statusCode");
                        JSONObject dt = response.getJSONObject("dt");

                        TextView tvLastPayment = (TextView) findViewById(R.id.tv_lastPayment);
                        TextView tvJarakWaktu = (TextView) findViewById(R.id.tv_jarakWaktu);
                        TextView tvJarak = (TextView) findViewById(R.id.tv_jarak);

                        if (dt.getInt("harga") == 0) {
                            tvLastPayment.setText("Rp. 0.000;");
                        } else {
                            tvLastPayment.setText("Rp. " + rupiahFormat.format(Double.parseDouble(dt.getString("harga"))) + ";");
                        }

                        if (dt.getInt("jarak") != 0) {
                            tvJarak.setText(dt.getString("jarak"));
                        } else {
                            tvJarak.setText("00 km");
                        }

                        if (!dt.getString("waktu").equals("")) {
                            tvJarakWaktu.setText(dt.getString("waktu"));
                        } else {
                            tvJarakWaktu.setText("00.00");
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(HomeActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if (loginAPI.loginAPI) {
                    perjalananTerakhir();
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Pendapatan terakhir tidak tampil. Periksa koneksi internetmu", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void pendapatanHariIni() {
        String url = AppConstans.APIPendapatan_Selfie + user.get(SessionManager.KEY_ID) + "&rentang=hari";
        String tag_json_obj = "pendapatan";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    TextView tvHarga = (TextView) findViewById(R.id.tv_harga);

                    if (status == 1) {
                        hideDialog();
                        JSONObject data=response.getJSONObject("dt");
                        int total= data.getInt("total");
                        if ( total != 0) {
                            tvHarga.setText("Rp. " + rupiahFormat.format(total) + ";");
                        } else {
                            tvHarga.setText("Rp. 0.000;");
                        }
                    } else {
                        hideDialog();

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(HomeActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if (loginAPI.loginAPI) {
                    pendapatanHariIni();
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Sisa saldo tidak tampil. Periksa koneksi internetmu", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void sisaSaldo() {
        String url = AppConstans.APISisaSaldo + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "saldo";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        int statusCode = response.getInt("statusCode");
                        String dt = String.valueOf(response.getString("dt"));

                        if(statusCode>0){
                            ConvertRupiah rp = new ConvertRupiah();
                            tvSaldo.setText("Rp. " + rp.ConvertRupiah(dt) + ";");
                        } else {
                            tvSaldo.setText("Rp. 0.000;");
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(HomeActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if (loginAPI.loginAPI) {
                    sisaSaldo();
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Sisa saldo tidak tampil. Periksa koneksi internetmu", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    public void ping() {
        String url = AppConstans.APIPing + user.get(SessionManager.KEY_ID) ;
        logManager.logD("url",url);
        String tag_json_obj = "ping";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG,"ping = "+ response.toString());

                    if(!response.isNull("dt")){
                        JSONObject data=response.getJSONObject("dt");
                        String orderId=data.getString("orderId");


                        Bundle b=new Bundle();
                        b.putString("orderId",orderId);
                        Intent intent=new Intent(HomeActivity.this, OrderViewActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void pingFood() {
        String url = AppConstans.APIPingFood + user.get(SessionManager.KEY_ID) ;
        logManager.logD("url",url);
        String tag_json_obj = "ping";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG, "ping food = "+ response.toString());

                    if(!response.isNull("dt")){
                        JSONObject data=response.getJSONObject("dt");
                        String orderId=data.getString("orderId");

                        logManager.logD(TAG,orderId);
                        Bundle b=new Bundle();
                        b.putString("orderId","food_"+orderId);
                        Intent intent=new Intent(HomeActivity.this, OrderViewActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        /*int id = item.getItemId();

        if (id == R.id.nav_history) {
            // Handle the camera action
        } else if (id == R.id.nav_topUp) {

        } else if (id == R.id.nav_langgananSaya) {

        } else if (id == R.id.nav_panduan) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == ivEditProfile) {
            startActivity(new Intent(HomeActivity.this, SuntingProfileActivity.class));
        } else if (v == tvSelfieOrder) {
            startActivity(new Intent(HomeActivity.this, SelfieOrderActivity.class));
        } else if (v == ivLocation) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 14));
            map.animateCamera(CameraUpdateFactory.zoomTo(14), 5000, null);
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        setNavigationDrawer();

        try {
            b = getIntent().getExtras();
            String message = b.getString("informasi");

            InformasiDialog informasiDialog = new InformasiDialog(this, message);

            if (statusInformasiDialog)
                informasiDialog.show();

        } catch (Throwable b) {

        }

        Log.i("called", "Activity --> onResume");
        //this.locationManager.requestLocationUpdates(this.locationProvider, 400, 1, this);






        perjalananTerakhir();
        sisaSaldo();
        pendapatanHariIni();
        /// cek rider statatus ///
        ping();
        pingFood();
    }

    @Override
    protected void onPause() {
        super.onPause();
        statusInformasiDialog = false;

        Log.i("called", "Activity --> onPause");
        //this.locationManager.removeUpdates(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        statusInformasiDialog = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        statusInformasiDialog = false;
    }


    //----------------------------------------
    //	Summary: For initializing the map
    //----------------------------------------
    private void initializeMap() {

        //set map type
        this.map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //TODO: other map initialization as needed
    }

    //-------------------------------------------
    //	Summary: initialize location manager
    //-------------------------------------------
    private void initializeLocationManager() {

        //get the location manager
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        //define the location manager criteria
        Criteria criteria = new Criteria();

        this.locationProvider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationProvider);


        //initialize the location
        if(location != null) {

            onLocationChanged(location);
        }
    }


    //------------------------------------------
    //	Summary: Location Listener  methods
    //------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        Log.i("called", "onLocationChanged");

        try {

            //when the location changes, update the map by zooming to the location
            LatLng my_position = new LatLng(location.getLatitude(), location.getLongitude());

            map.clear();
            map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(icon))
                    .position(my_position));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(my_position, 14));
            Log.i("called","Update map");
            SharedPreference.Save(HomeActivity.this, getString(R.string.my_coordinate), my_position);


        }catch (Throwable throwable){}
    }

    @Override
    public void onProviderDisabled(String arg0) {

        Log.i("called", "onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String arg0) {

        Log.i("called", "onProviderEnabled");
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

        Log.i("called", "onStatusChanged");
    }


    public void login(final String username, final String password) {
        String url = AppConstans.APILogin;
        String tag_json_obj = "json_obj_req";

        if (!((Activity) context).isFinishing()) {
            setDialog();
        }

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();

                        sessionManager.editor.clear();
                        sessionManager.editor.commit();

                        //int statusCode = response.getInt("statusCode");
                        //int expired = response.getInt("expired");

                        JSONObject data = response.getJSONObject("dt");

                        Session sessionModel = new Session();
                        sessionModel.set_id(data.getString("_id"));
                        sessionModel.setLatitude(data.getString("latitude"));
                        sessionModel.setLongitude(data.getString("longitude"));
                        sessionModel.setSenderId(data.getString("senderId"));
                        sessionModel.setUsername(data.getString("username"));
                        sessionModel.setLokasi(data.getString("lokasi"));
                        sessionModel.setNoPolisi(data.getString("noPolisi"));
                        sessionModel.setMerkKendaraan(data.getString("merkKendaraan"));
                        sessionModel.setTypeKendaraan(data.getString("typeKendaraan"));
                        sessionModel.setJenisKelamin(data.getString("jenisKelamin"));
                        sessionModel.setRole(data.getString("role"));
                        sessionModel.setNoTelp(data.getString("noTelp"));
                        sessionModel.setEmail(data.getString("email"));
                        sessionModel.setNama(data.getString("nama"));
                        sessionModel.setToken(response.getString("token"));
                        sessionModel.setSaldo(data.getString("saldo"));
                        sessionModel.setReady(data.getString("ready"));
                        sessionModel.setShow(data.getString("show"));
                        sessionModel.setFranchise(data.getString("franchise"));
                        sessionModel.setPassword(password);
                        sessionModel.setPhoto(data.getString("photo"));

                        if(!data.isNull("franchiseName")) sessionModel.setFranchiseName(data.getString("franchiseName"));
                        else sessionModel.setFranchiseName("franchiseName");

                        if(!data.isNull("rating")) sessionModel.setRating(data.getString("rating"));
                        else sessionModel.setRating("5");



                        sessionManager.createLoginSession(sessionModel);
                        user=sessionManager.getUserDetails();
                        if(user.get(SessionManager.KEY_READY).equals("true")){
                            sw_ready.setChecked(true);
                            container_switch_info.setVisibility(View.GONE);
                            tv_switch.setText("ON");
                        }else {
                            sw_ready.setChecked(false);
                            container_switch_info.setVisibility(View.VISIBLE);
                            tv_switch.setText("OFF");
                        }
                    } else {
                        hideDialog();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

                logManager.logE(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("password", password);
                params.put("senderId", AppConstans.token_gcm);

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}
