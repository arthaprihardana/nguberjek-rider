package com.driver.nguberjek.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.driver.nguberjek.HomeActivity;
import com.driver.nguberjek.R;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.Session;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LogManager;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmax.dialog.SpotsDialog;

/**
 * Created by masihsayang on 9/3/2016.
 */
public class LoginAPI {
    private final String TAG = LoginAPI.class.getSimpleName().toString();
    LogManager logManager = new LogManager();

    SpotsDialog dialog;

    List<Session> sessionList = new ArrayList<Session>();
    SessionManager sessionManager;

    Context context;
    Boolean isNewLogin;
    public boolean loginAPI = false;

    public LoginAPI(Context context, final String username, final String password, boolean isNewLogin) {
        sessionManager = new SessionManager(context.getApplicationContext());

        this.context = context;
        this.isNewLogin = isNewLogin;

        if (isNewLogin)
            this.dialog = new SpotsDialog(context, R.style.Custom);
        else {
            this.dialog = new SpotsDialog(context, R.style.DialogLogin);
        }

        login(username, password);
    }

    public void login(final String username, final String password) {
        String url = AppConstans.APILogin;
        String tag_json_obj = "json_obj_req";

        if (!((Activity) context).isFinishing()) {
            dialog.show();
        }

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();

                        if (!isNewLogin) {
                            sessionManager.editor.clear();
                            sessionManager.editor.commit();
                            loginAPI = true;
                        }

                        //int statusCode = response.getInt("statusCode");
                        //int expired = response.getInt("expired");

                        JSONObject data = response.getJSONObject("dt");

                        Session sessionModel = new Session();
                        sessionModel.set_id(data.getString("_id"));
                        sessionModel.setLatitude(data.getString("latitude"));
                        sessionModel.setLongitude(data.getString("longitude"));
                        sessionModel.setSenderId(data.getString("senderId"));
                        sessionModel.setUsername(data.getString("username"));
                        sessionModel.setLokasi(data.getString("lokasi"));
                        sessionModel.setNoPolisi(data.getString("noPolisi"));
                        sessionModel.setMerkKendaraan(data.getString("merkKendaraan"));
                        sessionModel.setTypeKendaraan(data.getString("typeKendaraan"));
                        sessionModel.setJenisKelamin(data.getString("jenisKelamin"));
                        sessionModel.setRole(data.getString("role"));
                        sessionModel.setNoTelp(data.getString("noTelp"));
                        sessionModel.setEmail(data.getString("email"));
                        sessionModel.setNama(data.getString("nama"));
                        sessionModel.setToken(response.getString("token"));
                        sessionModel.setSaldo(data.getString("saldo"));
                        sessionModel.setReady(data.getString("ready"));
                        sessionModel.setShow(data.getString("show"));
                        sessionModel.setPhoto(data.getString("photo"));
                        sessionModel.setPassword(password);
                        sessionModel.setFranchise(data.getString("franchise"));

                        if(!data.isNull("franchiseName")) sessionModel.setFranchiseName(data.getString("franchiseName"));
                        else sessionModel.setFranchiseName("franchiseName");

                        if(!data.isNull("rating")) sessionModel.setRating(data.getString("rating"));
                        else sessionModel.setRating("0");

                        sessionManager.createLoginSession(sessionModel);

                        if (isNewLogin) {
//                            ActivityManager.getInstance().finishAll();
                            context.startActivity(new Intent(context, HomeActivity.class));
                        }
                    } else {
                        hideDialog();

                        if (isNewLogin) {
                            View rootView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);

                            Snackbar snackbar = Snackbar
                                    .make((CoordinatorLayout) rootView.findViewById(R.id
                                            .coordinatorlayout), message, Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                            snackbar.show();
                        } else Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

                if (isNewLogin) {
                    View rootView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);

                    Snackbar snackbar = Snackbar
                            .make((CoordinatorLayout) rootView.findViewById(R.id
                                    .coordinatorlayout), R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                logManager.logE(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("password", password);
                params.put("senderId", AppConstans.token_gcm);

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void hideDialog() {
        if (dialog != null) {
            try {
                dialog.dismiss();
                dialog = null;
            }catch (Throwable throwable){};
        }
    }
}
