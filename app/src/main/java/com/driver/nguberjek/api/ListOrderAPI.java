package com.driver.nguberjek.api;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.NewOrderActivity;
import com.driver.nguberjek.R;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LogManager;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

/**
 * Created by sragen on 9/17/2016.
 */
public class ListOrderAPI {
    LogManager logManager = new LogManager();
    private final String TAG = ListOrderAPI.class.getSimpleName();

    SpotsDialog dialog;

    public ListOrderAPI(final Context context, final HashMap<String, String> user) {
        this.dialog = new SpotsDialog(context, R.style.ListOrder);

        String url = AppConstans.APIListOrder + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "listOrder";

        dialog.show();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    if (status == 1) {
                        hideDialog();

                        JSONArray data = response.getJSONArray("dt");

                        if (data.length() != 0) {
                            Intent i = new Intent(context, NewOrderActivity.class);
                            Bundle b = new Bundle();
                            b.putString("orderId", data.getJSONObject(0).getString("orderId"));
                            i.putExtras(b);
                            context.startActivity(i);
                        }
                    } else {
                        hideDialog();

                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(context, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
