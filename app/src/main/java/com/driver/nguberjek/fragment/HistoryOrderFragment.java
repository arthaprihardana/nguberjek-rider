package com.driver.nguberjek.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.R;
import com.driver.nguberjek.adapter.RiwayatAdapter;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.RiwayatModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LogManager;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class HistoryOrderFragment extends Fragment {
    private final String TAG = HistoryOrderFragment.class.getSimpleName();
    private LogManager logManager = new LogManager();

    private SessionManager sessionManager;
    private HashMap<String, String> user;

    private SpotsDialog dialog;

    ArrayList<RiwayatModel> riwayatModels;

    CoordinatorLayout coordinatorLayout;
    RecyclerView rvRiwayat;
    RelativeLayout rlNoRiwayatData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history_order, container, false);

        setContent(rootView);

        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();

        takeRiwayatData(rootView);

        return rootView;
    }

    public void setContent(View rootView){
        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id
                .coordinatorlayout);
        rvRiwayat = (RecyclerView) rootView.findViewById(R.id.rv_riwayat);
        rlNoRiwayatData = (RelativeLayout) rootView.findViewById(R.id.rl_noRiwayatData);
    }

    public void takeRiwayatData(final View rootView) {
        String url = AppConstans.APIRiwayat + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "history";
        Log.d("url",url);

        setDialog();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String msg = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    hideDialog();

                    if (status == 1) {
                        JSONArray data = response.getJSONArray("dt");

                        riwayatModels = new ArrayList<>();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dt = data.getJSONObject(i);

                            RiwayatModel riwayatModel = new RiwayatModel(dt);

                            riwayatModels.add(riwayatModel);

                            getData(rootView);
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(rootView.findViewById(R.id
                                        .coordinatorlayout), msg, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(getActivity(), user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);


                if (loginAPI.loginAPI) {
                    takeRiwayatData(rootView);
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    snackbar.show();

                    rvRiwayat.setVisibility(View.GONE);
                    rlNoRiwayatData.setVisibility(View.VISIBLE);
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void getData(View rootView) {
        rvRiwayat.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (riwayatModels.size() != 0) {
            rvRiwayat.setVisibility(View.VISIBLE);
            rlNoRiwayatData.setVisibility(View.GONE);

            RiwayatAdapter riwayatAdapter = new RiwayatAdapter(getActivity(), riwayatModels);
            rvRiwayat.setAdapter(riwayatAdapter);
        } else {
            rvRiwayat.setVisibility(View.GONE);
            rlNoRiwayatData.setVisibility(View.VISIBLE);
        }
    }

    public void setDialog() {
        this.dialog = new SpotsDialog(getActivity(), R.style.Custom);
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
