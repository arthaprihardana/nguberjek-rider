package com.driver.nguberjek;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.OrderDetailModel;
import com.driver.nguberjek.pushnotification.AudioPlayer;
import com.driver.nguberjek.pushnotification.SoundPoolPlayer;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by adikurniawan on 4/19/17.
 */

public class NewOrder_Activity extends BaseActivity {

    OrderDetailModel orderDetailModel;
    SoundPoolPlayer sound;

    // SOCKET APP //
    private Socket mSocket;

    AudioPlayer audio;
    Vibrator vibrator;

    String newOrderId="";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_order2);
        this.setFinishOnTouchOutside(false);

        sound = new SoundPoolPlayer(this);


        Bundle b = getIntent().getExtras();
        AppConstans.orderID = b.getString("orderId");

        /// SOCKET ///
        try {
            mSocket = IO.socket(AppConstans.SOCKET_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        //////////////

        try {
            audio= new AudioPlayer();
            audio.play(this, R.raw.mission);

            // vibrate the device
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(1*60*1000);
        } catch (Throwable throwable) {
        }

        Log.d("orderId",AppConstans.orderID);
        if(AppConstans.orderID.contains("food_")) {
            newOrderId=AppConstans.orderID.replace("food_","");
            get_detail_order_food();
        }else{
            get_detail_order();
        }
    }

    public void playCustomSound(){
        sound.playShortResource(R.raw.mission);
    }

    public void get_detail_order_food() {
        String url = AppConstans.APIOrderDetail + AppConstans.orderID;
        String tag_json_obj = "tempOrder";
        Log.d("url",url);

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("response",response.toString());
                    int status = response.getInt("s");
                    String message = response.getString("msg");
                    if (status == 1) {
                        JSONArray dataArray = response.getJSONArray("dt");
                        if(dataArray.length()>0) {
                            JSONObject data = dataArray.getJSONObject(0);
                            orderDetailModel = new OrderDetailModel(data,true);

                            setContent(true);
                        }
                    } else {
                        Toast.makeText(NewOrder_Activity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("response", e.getMessage());

                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrder_Activity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    get_detail_order_food();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrder_Activity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    public void get_detail_order() {
        String url = AppConstans.APIOrderDetail + AppConstans.orderID;
        String tag_json_obj = "tempOrder";
        Log.d("url",url);

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("response",response.toString());
                    int status = response.getInt("s");
                    String message = response.getString("msg");
                    if (status == 1) {
                        JSONArray dataArray = response.getJSONArray("dt");
                        if(dataArray.length()>0) {
                            JSONObject data = dataArray.getJSONObject(0);
                            orderDetailModel = new OrderDetailModel(data);

                            setContent(false);
                        }
                    } else {
                         Toast.makeText(NewOrder_Activity.this, message, Toast.LENGTH_SHORT).show();
                         finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("response", e.getMessage());

                    Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(NewOrder_Activity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    get_detail_order();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(NewOrder_Activity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new




                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void setContent(final boolean isFood) {
        TextView tvTitle=(TextView)findViewById(R.id.tvTitle);
        TextView tvNama = (TextView) findViewById(R.id.tv_nama);
        TextView tvDari = (TextView) findViewById(R.id.tv_dari);
        TextView tvKe = (TextView) findViewById(R.id.tv_ke);
        TextView tvHarga = (TextView) findViewById(R.id.tv_harga);

        Button btnTerima = (Button) findViewById(R.id.btn_terima);
        Button btnTolak = (Button) findViewById(R.id.btn_tolak);

        
        if(isFood){
            tvTitle.setText("Order Masuk (NGUBERFOOD)");
        }else {
            tvTitle.setText("Order Masuk");
        }
        tvNama.setText(orderDetailModel.nama);
        tvDari.setText(orderDetailModel.lokasiAwal);
        tvKe.setText(orderDetailModel.lokasiTujuan);
        tvHarga.setText("Rp. " + rupiahFormat.format(Double.parseDouble(orderDetailModel.harga)) + ";");

        btnTerima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFood){
                    emit_confirm_food_order("accept");
                }else {
                    emit_confirm_order("accept");
                }
                //Toast.makeText(getApplicationContext(),"terima",Toast.LENGTH_SHORT).show();
            }
        });
        btnTolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.cancel();
                audio.stop();

                ActivityManager.getInstance().finishAll();
                finish();
                startActivity(new Intent(NewOrder_Activity.this, HomeActivity.class));
                //emit_confirm_order("reject");
                //Toast.makeText(getApplicationContext(),"tolak",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        socketOn();
    }

    @Override
    protected void onPause() {
        super.onPause();
        socketOff();
    }

    private void socketOn() {
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("server-response: confirm order", onConfirmOrder);
        mSocket.on("server-response: confirm food order", onConfirmOrder);
        mSocket.on("server-response: order request", onOrderRequest);
        //mSocket.on("server-response: cancel order", onCancelOrder);
        mSocket.connect();
    }

    private void socketOff() {
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("server-response: confirm order", onConfirmOrder);
        mSocket.off("server-response: confirm food order", onConfirmOrder);
        mSocket.on("server-response: order request", onOrderRequest);
        //mSocket.off("server-response: cancel order", onCancelOrder);
    }


    private void emit_confirm_order(String status) {
        //socketOff();
        //socketOn();
        JSONObject data = new JSONObject();
        try {
            data.put("token", user.get(SessionManager.KEY_TOKEN));
            data.put("order_id", AppConstans.orderID);
            data.put("rider_id", user.get(SessionManager.KEY_ID));
            data.put("status_confirm", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: confirm order", data);
        Log.d("SOCKET", "client-request: confirm order");
        Log.d("SOCKET", data.toString());
    }

    private void emit_confirm_food_order(String status) {
        //socketOff();
        //socketOn();
        JSONObject data = new JSONObject();
        try {
            data.put("token", user.get(SessionManager.KEY_TOKEN));
            data.put("order_id", newOrderId);
            data.put("rider_id", user.get(SessionManager.KEY_ID));
            data.put("status_confirm", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: confirm food order", data);
        Log.d("SOCKET", "client-request: confirm food order");
        Log.d("SOCKET", data.toString());
    }


    private Emitter.Listener onConfirmOrder = new Emitter.Listener(){
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET", "server-response: confirm order");
                        Log.d("SOCKET", response.toString());

                        if(!response.isNull("order_id")){
                            ActivityManager.getInstance().finishAll();
                            finish();
                            startActivity(new Intent(NewOrder_Activity.this, HomeActivity.class));
                        }else{
                            String msg=response.getString("msg");
                            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();

                            ActivityManager.getInstance().finishAll();
                            finish();
                            startActivity(new Intent(NewOrder_Activity.this, HomeActivity.class));
                        }
                        vibrator.cancel();
                        audio.stop();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
                        return;
                    }


                }
            });
        }
    };

    private Emitter.Listener onOrderRequest = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET","server-response: order request");
                        Log.d("SOCKET",response.toString());

                        String status = response.getString("s");
                        String message = response.getString("message");



                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }


                }
            });
        }
    };

    private Emitter.Listener onCancelOrder = new Emitter.Listener(){
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET", "server-response: cancel order");
                        Log.d("SOCKET", response.toString());

                        if(!response.isNull("msg")) {
                            String message = response.getString("msg");
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }


                }
            });
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
