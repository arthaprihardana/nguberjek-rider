package com.driver.nguberjek.utilities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.HomeActivity;
import com.driver.nguberjek.R;
import com.driver.nguberjek.TrackingOrderActivity;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.NguberjekRiderDialog;
import com.driver.nguberjek.model.OrderDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

/**
 * Created by masihsayang on 9/6/2016.
 */
public class PerubahanStatusOrderAPI {
    LogManager logManager = new LogManager();
    private final String TAG = "updateStatusOrder";

    SpotsDialog dialog;
    NguberjekRiderDialog nguberjekRiderDialog;

    SessionManager sessionManager;

    OrderDetailModel orderDetailModel;

    public PerubahanStatusOrderAPI(Context context, String status, OrderDetailModel orderDetailModel, HashMap<String, String> user) {
        this.dialog = new SpotsDialog(context, R.style.Custom);
        this.orderDetailModel = orderDetailModel;

        this.sessionManager = new SessionManager(context.getApplicationContext());
        perubahanStatusOrderAPI(context, status, user);
    }

    private void perubahanStatusOrderAPI(final Context context, final String status, final HashMap<String, String> user) {
        String url = AppConstans.APIStatusOrder;
        String tag_json_obj = "updateStatusOrder";

        dialog.show();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG, response.toString());

                    int s = response.getInt("s");
                    final String message = response.getString("msg");


                    if (s == 1) {
                        if (status.equals("3")) {
                            hideDialog();

                            Bundle b = new Bundle();
                            b.putSerializable("ORDER_DETAIL", orderDetailModel);
                            b.putString("status", "1");
                            Intent intent = new Intent(context, TrackingOrderActivity.class);
                            intent.putExtras(b);

                            ActivityManager.getInstance().finishAll();
                            context.startActivity(intent);
                        } else if (status.equals("7")) {
                            hideDialog();

                            Intent intent = new Intent(context, HomeActivity.class);

                            ActivityManager.getInstance().finishAll();
                            context.startActivity(intent);
                        } else if (status.equals("5")) {
                            checkOrder(context, user);
                        } else if (status.equals("4")) {
                            hideDialog();

                            NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

                            Bundle b = new Bundle();

                            b.putString("informasi", "Perjalanan Selesai\nTotal harga perjalanan anda Rp. " + rupiahFormat.format(Double.parseDouble(orderDetailModel.harga)) + ";");
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtras(b);

                            ActivityManager.getInstance().finishAll();
                            context.startActivity(intent);
                        }
                    } else {
                        hideDialog();

                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(context, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                logManager.logE(TAG, "Error: " + error.getMessage());
                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("orderId", orderDetailModel.orderId);
                params.put("status", status);
                params.put("riderId", user.get(SessionManager.KEY_ID));
                params.put("komentar","");
                params.put("rating","");

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void checkOrder(final Context context, final HashMap<String, String> user) {
        String url = AppConstans.APIOrderDetail + orderDetailModel.orderId;
        String tag_json_obj = "tempOrder";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");

                    Log.d(TAG, response.toString());

                    if (status == 1) {
                        JSONObject data = response.getJSONObject("dt");

                        orderDetailModel = new OrderDetailModel(data);

                        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

                        Bundle b = new Bundle();
                        b.putString("informasi", "Terjadi Hambatan, Perjalanan tidak sampai tujuan. Pembayaran dipotong 50% menjadi Rp. " + rupiahFormat.format(Double.parseDouble(orderDetailModel.harga)) + ";");
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtras(b);

                        ActivityManager.getInstance().finishAll();
                        context.startActivity(intent);
                    } else {
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    Log.d(TAG, e.getMessage());

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(context, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}