package com.driver.nguberjek.utilities;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by adikurniawan on 5/5/17.
 */

public class ConvertRupiah {
    public String ConvertRupiah(String value){
        String sallary = "";
        if (value.equals("")) {
            sallary = "0";
        } else {
            sallary = value;
        }
        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(sallary));
        return rupiah;
    }
}
