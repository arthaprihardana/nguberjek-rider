package com.driver.nguberjek.utilities;

import android.util.Log;

import com.driver.nguberjek.app.AppConstans;

/**
 * Created by Nur If Alan on 29/08/2016.
 */
public class LogManager {

    public void logD(String tag,String message){
        if(!AppConstans.isRelease){
            Log.d(tag,message);
        }
    }
    public void logI(String tag,String message){
        if(!AppConstans.isRelease){
            Log.d(tag,message);
        }
    }
    public void logW(String tag,String message){
        if(!AppConstans.isRelease){
            Log.d(tag,message);
        }
    }
    public void logE(String tag,String message){
        if(!AppConstans.isRelease){
            Log.d(tag,message);
        }
    }
}
