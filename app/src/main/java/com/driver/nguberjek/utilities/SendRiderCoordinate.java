package com.driver.nguberjek.utilities;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.R;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.app.SharedPreference;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Codelabs on 16/06/2016.
 */
public class SendRiderCoordinate {
    private final String TAG = SendRiderCoordinate.class.getSimpleName();
    Activity context;

    LogManager logManager = new LogManager();

    CountDownTimer countDownTimer;

    HashMap<String, String> user;

    double latitude, longitude;

    // SOCKET APP //
    private Socket mSocket;

    public SendRiderCoordinate(Activity context, HashMap<String, String> user) {
        this.context = context;
        this.user = user;

        try {
            mSocket = IO.socket(AppConstans.SOCKET_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        socketOn();
        emit_sendCordinate();
//        insertPartnerCoordinate();
    }


    private void socketOn(){
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("server-response: gps driver", onSendCordinate);
        mSocket.connect();
    }

    private void socketOff(){
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT,onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("server-response: gps driver", onSendCordinate);
    }

    private void emit_sendCordinate(){
        JSONObject data=new JSONObject();
        try {

            LatLng my_coorinate ;
            my_coorinate = (LatLng) SharedPreference.Get(context, context.getString(R.string.my_coordinate), LatLng.class);
            if(my_coorinate==null){
                my_coorinate = new LatLng(-6.170933, 106.826830);
            }
            data.put("_id", user.get(SessionManager.KEY_ID));
            data.put("koordinat", my_coorinate.longitude + "," + my_coorinate.latitude);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: gps driver", data);
        Log.d("SOCKET","client-request: gps driver");
        Log.d("SOCKET",data.toString());

    }

    private Emitter.Listener onSendCordinate = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET","server-response: gps driver");
                        Log.d("SOCKET",response.toString());

                        cancelCountDown();
                        setCountDown();



                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                }
            });
        }
    };

    public void getCurrentLocation() {
        GPSTracker gps = new GPSTracker(context);
        if (gps.canGetLocation()) { // gps enabled
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            //insertPartnerCoordinate();
//            emit_sendCordinate();
            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            //gps.stopUsingGPS();
        }
    }

    public void insertPartnerCoordinate() {
        String url = AppConstans.APIKoordinat + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "koordinat";

        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    Log.d(TAG, response.toString());

                    cancelCountDown();
                    setCountDown();

                    if (status == 1) {
                        
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "NGUBERJEK: You don't send your location", Toast.LENGTH_SHORT).show();
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                cancelCountDown();
                setCountDown();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                Log.d(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {
                    LatLng my_coorinate ;
                    my_coorinate = (LatLng) SharedPreference.Get(context, context.getString(R.string.my_coordinate), LatLng.class);
                    if(my_coorinate==null){
                        my_coorinate = new LatLng(-6.170933, 106.826830);
                    }
                    params.put("koordinatTerakhir", my_coorinate.longitude + "," + my_coorinate.latitude);
                    Log.d(TAG, params.toString());
                }catch (Throwable throwable){}

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void setCountDown() {

        countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("CountDown",millisUntilFinished/1000+"");
            }

            @Override
            public void onFinish() {
//                insertPartnerCoordinate();
                emit_sendCordinate();
                //Do what you want
            }
        };
        countDownTimer.start();
    }

    public void cancelCountDown() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    //////////////////////////////////////// SOCKET APP //////////////////////////////////////////////
    public Boolean isConnected = true;
    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {
                        //Toast.makeText(getApplicationContext(), R.string.connect, Toast.LENGTH_LONG).show();
                        Log.d("socket",context.getString(R.string.connect));
                        isConnected = true;
                    }
                }
            });
        }
    };

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    //Toast.makeText(getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                    Log.d("socket",context.getString(R.string.disconnect));

                }
            });
        }
    };

    public Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                    Log.d("socket",context.getString(R.string.error_connect));

                }
            });
        }
    };

}
