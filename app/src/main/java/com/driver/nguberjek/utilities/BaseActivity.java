package com.driver.nguberjek.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.driver.nguberjek.HomeActivity;
import com.driver.nguberjek.app.SharedPreference;
import com.google.android.gms.maps.model.LatLng;
import com.driver.nguberjek.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Nur If Alan on 29/08/2016.
 */
public class BaseActivity extends AppCompatActivity {
    private final String TAG = BaseActivity.class.getSimpleName();

    public LogManager logManager = new LogManager();

    public CoordinatorLayout coordinatorLayout;

    public SessionManager sessionManager;
    public HashMap<String, String> user;

    private static Map<Class<? extends Activity>, Activity> launched = new HashMap<Class<? extends Activity>, Activity>();
    protected Context context;
    protected Activity mActivity;

    protected int viewHeight;
    protected int viewWidth;

    public NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

    protected SpotsDialog dialog;

    protected SendRiderCoordinate sendRiderCoordinate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Activity activity = launched.get(this.getClass());
        if (activity != null)
            activity.finish();

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();

        launched.put(this.getClass(), this);

        super.onCreate(savedInstanceState);
        context = this;
        mActivity = this;
        ViewUtils.GetInstance().setResolution(this);

        viewHeight = ViewUtils.GetInstance().getHeight();
        viewWidth = ViewUtils.GetInstance().getWidth();

        ActivityManager.getInstance().onCreate(this);
    }

    final private int REQUEST_CODE_GPS_PERMISSION = 123;

    private void gpsPermission() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_GPS_PERMISSION);
            return;
        }
        //this.sendRiderCoordinate = new SendRiderCoordinate(context, user);
        send_myCoordinate();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_GPS_PERMISSION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        //this.sendRiderCoordinate = new SendRiderCoordinate(context, user);
                        send_myCoordinate();
                    } else {
                        // Permission Denied
                        Toast.makeText(this, "You Must Allow Permission", Toast.LENGTH_SHORT).show();
                        Toast.makeText(this, "for Update Your Location", Toast.LENGTH_SHORT).show();

                        ActivityManager.getInstance().finishAll();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onDestroy() {
        Activity activity = launched.get(this.getClass());

        if (activity == this)
            launched.remove(this.getClass());

        ActivityManager.getInstance().onDestroy(this);

        hideDialog();

        if(sendRiderCoordinate != null) {
            sendRiderCoordinate.cancelCountDown();
        }


        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resettingView();
    }

    protected void resettingView() {
        ViewUtils.GetInstance().setResolution(this);
        viewHeight = ViewUtils.GetInstance().getHeight();
        viewWidth = ViewUtils.GetInstance().getWidth();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resettingView();
    }


    public void setHideKeyboardOnTouch(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(mActivity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setHideKeyboardOnTouch(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }catch (Throwable d){}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void setCoordinate() {
        gpsPermission();
    }

    public void setDialog() {
        this.dialog = new SpotsDialog(context, R.style.Custom);
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public LatLng getCurrentLocation() {
        double latitude = 0, longitude = 0;

        GPSTracker gps = new GPSTracker(context);
        /*if (gps.canGetLocation()) { // gps enabled
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            latitude = -6.170933;
            longitude = 106.826830;
            //gps.stopUsingGPS();
        }*/


        LatLng my_coordinate;
        if (gps.canGetLocation()) { // gps enabled
        } else {
            gps.showSettingsAlert();
        }
        my_coordinate=(LatLng) SharedPreference.Get(context, context.getString(R.string.my_coordinate), LatLng.class);
        if (my_coordinate == null) {
            my_coordinate = new LatLng(-6.170933, 106.826830);
        }

        return my_coordinate;
    }


    public void send_myCoordinate() {
        GPSTracker gps = new GPSTracker(context);
        if (gps.canGetLocation()) { // gps enabled
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            LatLng my_position=new LatLng(latitude,longitude);
            SharedPreference.Save(context, getString(R.string.my_coordinate), my_position);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            //gps.stopUsingGPS();
        }
    }

    //////////////////////////////////////// SOCKET APP //////////////////////////////////////////////
    public Boolean isConnected = true;
    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {
                        //Toast.makeText(getApplicationContext(), R.string.connect, Toast.LENGTH_LONG).show();
                        Log.d("socket",getString(R.string.connect));
                        isConnected = true;
                    }
                }
            });
        }
    };

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    //Toast.makeText(getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                    Log.d("socket",getString(R.string.disconnect));

                }
            });
        }
    };

    public Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                    Log.d("socket",getString(R.string.error_connect));

                }
            });
        }
    };





}
