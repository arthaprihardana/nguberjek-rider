package com.driver.nguberjek.utilities;

/**
 * Created by Nur If Alan on 29/08/2016.
 */
public class Application extends android.app.Application {


    private static Application instance;


    public static Application getInstance() {
        if (instance == null)
            throw new IllegalStateException();

        return instance;
    }

    public Application() {
        instance = this;
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }

    public void closeApplication(){
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//		 BitmapAjaxCallback.clearCache();
    }
}

