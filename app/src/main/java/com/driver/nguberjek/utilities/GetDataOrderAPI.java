package com.driver.nguberjek.utilities;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.R;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.OrderDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

/**
 * Created by masihsayang on 9/13/2016.
 */
public class GetDataOrderAPI {
    private final String TAG = "getDataOrderAPI";
    SpotsDialog dialog;

    public OrderDetailModel GetDataOrderAPI(final Context context, String orderId, final HashMap<String, String> user) {
        this.dialog = new SpotsDialog(context, R.style.Custom);

        String url = AppConstans.APIOrderDetail + orderId;
        String tag_json_obj = "tempOrder";

        if(!((Activity) context).isFinishing())
        {
            dialog.show();
        }

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    Log.d(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();
                        JSONObject data = response.getJSONObject("dt");

//                        return new OrderDetailModel(data);
                    } else {
                        hideDialog();

                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    Log.d(TAG, e.getMessage());

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(context, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

        return null;
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
