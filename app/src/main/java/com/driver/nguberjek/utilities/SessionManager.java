package com.driver.nguberjek.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.driver.nguberjek.HomeActivity;
import com.driver.nguberjek.MainActivity;
import com.driver.nguberjek.model.Session;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Nur If Alan on 29/08/2016.
 */
public class SessionManager {
    List<Session> sessionList;

    SharedPreferences pref;

    public SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "PaxPartnerPref";

    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_ID = "_id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_SENDER_ID = "sender_id";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_DEVICE_NAME = "device_name";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_LOKASI = "lokasi";
    public static final String KEY_NO_POLISI = "no_polisi";
    public static final String KEY_MERK_KENDARAAN = "merk_kendaraan";
    public static final String KEY_TYPE_KENDARAAN = "type_kendaraan";
    public static final String KEY_JENIS_KELAMIN = "jenis_kelamin";
    public static final String KEY_ROLE = "role";
    public static final String KEY_NO_TELP = "no_telp";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAMA = "name";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_SALDO = "saldo";
    public static final String KEY_READY = "ready";
    public static final String KEY_SHOW = "show";
    public static final String KEY_FRANCHISE = "franchise";
    public static final String KEY_FRANCHISE_NAME = "franchise_name";
    public static final String KEY_PHOTO = "photo";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_RATING = "rating";

    public SessionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(Session list){
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_ID, list.get_id());
        editor.putString(KEY_LATITUDE, list.getLatitude());
        editor.putString(KEY_LONGITUDE, list.getLongitude());
        editor.putString(KEY_SENDER_ID, list.getSenderId());
        editor.putString(KEY_DEVICE_ID, list.getDeviceId());
        editor.putString(KEY_DEVICE_NAME, list.getDeviceName());
        editor.putString(KEY_USERNAME, list.getUsername());
        editor.putString(KEY_LOKASI, list.getLokasi());
        editor.putString(KEY_NO_POLISI, list.getNoPolisi());
        editor.putString(KEY_MERK_KENDARAAN, list.getMerkKendaraan());
        editor.putString(KEY_TYPE_KENDARAAN, list.getTypeKendaraan());
        editor.putString(KEY_JENIS_KELAMIN, list.getJenisKelamin());
        editor.putString(KEY_ROLE, list.getRole());
        editor.putString(KEY_NO_TELP, list.getNoTelp());
        editor.putString(KEY_EMAIL, list.getEmail());
        editor.putString(KEY_NAMA, list.getNama());
        editor.putString(KEY_TOKEN, list.getToken());
        editor.putString(KEY_SALDO, list.getSaldo());
        editor.putString(KEY_READY, list.getReady());
        editor.putString(KEY_SHOW, list.getShow());
        editor.putString(KEY_FRANCHISE, list.getFranchise());
        editor.putString(KEY_FRANCHISE_NAME, list.getFranchiseName());
        editor.putString(KEY_PHOTO, list.getPhoto());
        editor.putString(KEY_RATING, list.getRating());
        editor.putString(KEY_PASSWORD, list.getPassword());

        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public int checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(context, HomeActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(i);
            return 0;
        }else return 1;
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_LATITUDE, pref.getString(KEY_LATITUDE, null));
        user.put(KEY_LONGITUDE, pref.getString(KEY_LONGITUDE, null));
        user.put(KEY_SENDER_ID, pref.getString(KEY_SENDER_ID, null));
        user.put(KEY_DEVICE_ID, pref.getString(KEY_DEVICE_ID, null));
        user.put(KEY_DEVICE_NAME, pref.getString(KEY_DEVICE_NAME, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_LOKASI, pref.getString(KEY_LOKASI, null));
        user.put(KEY_NO_POLISI, pref.getString(KEY_NO_POLISI, null));
        user.put(KEY_MERK_KENDARAAN, pref.getString(KEY_MERK_KENDARAAN, null));
        user.put(KEY_TYPE_KENDARAAN, pref.getString(KEY_TYPE_KENDARAAN, null));
        user.put(KEY_JENIS_KELAMIN, pref.getString(KEY_JENIS_KELAMIN, null));
        user.put(KEY_ROLE, pref.getString(KEY_ROLE, null));
        user.put(KEY_NO_TELP, pref.getString(KEY_NO_TELP, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_NAMA, pref.getString(KEY_NAMA, null));
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));
        user.put(KEY_SALDO, pref.getString(KEY_SALDO, null));
        user.put(KEY_READY, pref.getString(KEY_READY, null));
        user.put(KEY_SHOW, pref.getString(KEY_SHOW, null));
        user.put(KEY_FRANCHISE, pref.getString(KEY_FRANCHISE, null));
        user.put(KEY_FRANCHISE_NAME, pref.getString(KEY_FRANCHISE_NAME, null));
        user.put(KEY_PHOTO, pref.getString(KEY_PHOTO, null));
        user.put(KEY_RATING, pref.getString(KEY_RATING, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));

        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, MainActivity.class);
        ActivityManager.getInstance().finishAll();

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }
}