package com.driver.nguberjek.app;

import com.driver.nguberjek.model.AddressModel;

/**
 * Created by Codelabs on 24/08/2016.
 */
public class AppConstans {
    public static String token_gcm = "";

    public static final String APIHost="http://159.203.109.20:8080/api/v3/";
    public static final String APIHost_old="http://159.203.109.20/";	// PRODUCTION
    public static final String SOCKET_SERVER_URL = "http://159.203.109.20:8080";

    /*public static final String APIHost="http://192.168.43.173:8080/api/v3/";
    public static final String APIHost_old="http://192.168.43.173/";	// PRODUCTION
    public static final String SOCKET_SERVER_URL = "http://192.168.43.173:8080";*/

    public static final String APILogin=APIHost+"lgn/driver";
    public static final String APIForgotPassword=APIHost+"lupaPassword/driver";
    public static final String APILogout=APIHost+"ns/lgo/driver?_id=";
    public static final String APISisaSaldo=APIHost+"ns/saldo/rider?riderId=";
    public static final String APIRiwayat=APIHost+"ns/history?riderId=";
    public static final String APILastOrder=APIHost+"ns/lastorder?riderId=";
    public static final String APIPendapatan=APIHost+"ns/pendapatanOrder?riderId=";
    public static final String APIPendapatan_Selfie=APIHost+"ns/totalPendapatan?driverId=";
    public static final String APIKoordinat=APIHost+"api/v1/koordinat/rider/";
    public static final String APIAcceptOrder=APIHost+"ws/acceptOrder";
    public static final String APIOrderDetail=APIHost+"ns/orderDetail?orderId=";
    public static final String APIOrderDetailFood=APIHost+"ns/riwayatFoodOrder?orderId=";
    public static final String APIRejectORder=APIHost+"api/v1/rejectOrder";
    public static final String APIStatusOrder=APIHost+"ns/updateStatusOrder";
    public static final String APIUpdateDataRider=APIHost+"ns/driver?_id=";
    public static final String APIListOrder=APIHost+"api/v1/listOrder?riderId=";
    public static final String APIFare =APIHost+"ns/fare";
    public static final String APISelfieOrder =APIHost+"ns/selfie";
    public static final String APISelfieOrderUpdate =APIHost+"ns/status_perjalanan_selfie";
    public static final String APIDataSelfieOrder =APIHost+"ns/selfie?orderId=";
    public static final String APIRiwayatSelfieOrder =APIHost+"ns/selfie?riderId=";
    public static final String APIChatList = APIHost+"ws/listChat";
    public static final String APIChatSend = APIHost+"ws/sendChat";
    public static final String APIUpload = APIHost+"ns/upload/";
    public static final String APIPing = APIHost+"ping/order/";
    public static final String APIPingFood = APIHost+"ping/foodorder/";


    /*public static final String APILogin=APIHost+"loginrider";
    public static final String APIForgotPassword=APIHost+"lupapassword/rider";
    public static final String APILogout=APIHost+"api/v1/logoutrider?riderId=";
    public static final String APISisaSaldo=APIHost+"api/v1/saldo/rider?riderId=";
    public static final String APIRiwayat=APIHost+"api/v1/history?riderId=";
    public static final String APILastOrder=APIHost+"api/v1/lastorder?riderId=";
    public static final String APIPendapatan=APIHost+"api/v1/pendapatan/rider?riderId=";
    public static final String APIKoordinat=APIHost+"api/v1/koordinat/rider/";
    public static final String APIAcceptOrder=APIHost+"api/v1/acceptOrder";
    public static final String APIOrderDetail=APIHost+"api/v1/tempOrder?orderId=";
    public static final String APIRejectORder=APIHost+"api/v1/rejectOrder";
    public static final String APIStatusOrder=APIHost+"api/v1/updateStatusOrder";
    public static final String APIUpdateDataRider=APIHost+"api/v1/rider?_id=";
    public static final String APIListOrder=APIHost+"api/v1/listOrder?riderId=";
    public static final String APIFare =APIHost+"api/v1/fare";
    public static final String APISelfieOrder =APIHost+"api/v1/selfie";
    public static final String APIDataSelfieOrder =APIHost+"api/v1/selfie?orderId=";
    public static final String APIRiwayatSelfieOrder =APIHost+"api/v1/selfie?riderId=";
    public static final String APIChatList = APIHost+"api/v1/listChat";
    public static final String APIChatSend = APIHost+"api/v1/sendChat";*/

    /**
     * TopUp API VERITRANS
     */
    public static final String APITopUpCharge=APIHost_old+"api/v1/payment/charge";
    public static final String APITopUpStatus=APIHost_old+"api/v1/payment/stats/";
    public static final String APITopUpDB=APIHost_old+"api/v1/payment/db?riderId=";

    public static boolean isRelease = false;    // set true if release
    public static boolean cancelOrder = false;
    public static String orderID = "";
    public static boolean firstTimeChat=true;

    public static AddressModel addressModel;

}
