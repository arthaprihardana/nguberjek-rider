package com.driver.nguberjek;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener{
    private final String TAG = ForgotPasswordActivity.class.getSimpleName();

    Button btnSend;
    EditText etEmailForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        setActionBar();
        setContent();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Lupa Password?");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setContent() {
        btnSend = (Button) findViewById(R.id.btn_kirimForgotPassword);
        etEmailForgotPassword = (EditText) findViewById(R.id.et_emailForgotPassword);

        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnSend){
            hideSoftKeyboard(ForgotPasswordActivity.this);

            if (etEmailForgotPassword.getText().toString().trim().length() > 0)
                insertForgot(etEmailForgotPassword);
            else
                etEmailForgotPassword.setError("This Field is Required");
        }
    }

    public void insertForgot(final EditText etEmailForgotPassword) {
        String url = AppConstans.APIForgotPassword;
        String tag_json_obj = "forgot_password";
        Log.d("url",url);

        setDialog();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();

                        Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();

                        finish();
                    } else {
                        hideDialog();
                        String err = response.getString("err");
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, err, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();

//                        paxPartnerDialog = new PaxPartnerDialog(ForgotPasswordActivity.this, status, message, new PaxPartnerDialog.OnDialogConditionListener() {
//                            @Override
//                            public void onDialogSelect(boolean positive) {
//                            }
//                        });
//                        paxPartnerDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                snackbar.show();
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", etEmailForgotPassword.getText().toString());

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer .MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
