package com.driver.nguberjek;

import android.*;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.adapter.ChatAdapter;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.DialogCancelOrder;
import com.driver.nguberjek.model.ChatModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LogManager;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sragen on 8/20/2016.
 */
public class ChatActivity extends BaseActivity {
    public final String TAG = ChatActivity.class.getSimpleName().toString();
    LogManager logManager = new LogManager();

    Toolbar toolbar;
    String orderId;
    ArrayList<ChatModel> chatList=new ArrayList<ChatModel>();

    ChatAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar, loadingchat;
    private EditText messageEditText;

    public static boolean isActive = false;
    public String telepon="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        try {
            Bundle b=getIntent().getExtras();
            orderId=b.getString("orderId");
            telepon=b.getString("telepon");
        }catch (Throwable throwable){}

        setToolBar();
        setContent();
        if(orderId!=null) getChat();
        setNavatgation();


        if(AppConstans.firstTimeChat) firstMessage();
    }

    private void firstMessage(){
        AppConstans.firstTimeChat=false;
        DialogCancelOrder dial = new DialogCancelOrder(context, new DialogCancelOrder.ListenerDialog() {
            @Override
            public void onSelected(boolean canInput,String value) {
                if(canInput) {
                    messageEditText.setText(value);
                    sendChat();
                }
            }
        });
        dial.show();
    }


    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            /*ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);*/
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            /*imgLogo.setVisibility(View.GONE);*/
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Chatting");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    public void setContent(){
        recyclerView = (RecyclerView) findViewById(R.id.rv_chat);
        messageEditText = (EditText) findViewById(R.id.et_message);
        progressBar=(ProgressBar) findViewById(R.id.loading_list_chat);
        loadingchat=(ProgressBar) findViewById(R.id.loading_chat);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new ChatAdapter(this,chatList);
        recyclerView.setAdapter(adapter);
    }

    public void setNavatgation(){
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(messageEditText.getText().length()>0){
                    sendChat();
                }else{
                    messageEditText.setError("masukkan pesan anda");
                }
            }
        });
        findViewById(R.id.iv_telepon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callingPermission();
            }
        });
    }

    final private int REQUEST_CODE_CALLING_PERMISSION = 1;
    private void callingPermission() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_CALLING_PERMISSION);
            return;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telepon));
        if (ActivityCompat.checkSelfPermission(ChatActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    private void addItem(){
        chatList.add(new ChatModel(messageEditText.getText().toString()));
        adapter.notifyDataSetChanged();
        messageEditText.setText("");
    }

    private void sendChat(){
        final String url= AppConstans.APIChatSend;
        logManager.logD(TAG, url);
        String tag_json_obj="sendChat";

        loadingchat.setVisibility(View.VISIBLE);
        messageEditText.setPadding(10, 10, 40, 10);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG, response.toString());
                    String status=response.getString("s");
                    addItem();
                    loadingchat.setVisibility(View.GONE);
                    messageEditText.setPadding(10, 10, 10, 10);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    loadingchat.setVisibility(View.GONE);
                    messageEditText.setPadding(10, 10, 10, 10);
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                loadingchat.setVisibility(View.GONE);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_TOKEN) );
                logManager.logD(TAG, params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId",orderId);
                params.put("pengirim","2");
                params.put("message",messageEditText.getText().toString());
                logManager.logD(TAG, params.toString());
                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void getChat(){
        final String url= AppConstans.APIChatList+"?orderId="+orderId;
        logManager.logD(TAG, url);
        String tag_json_obj="getChatLIst";


        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG, response.toString());
                    String status=response.getString("s");

                    if(status.equals("1")){
                        JSONArray dataAr=response.getJSONArray("dt");
                        for(int i=0;i<dataAr.length();i++){
                            JSONObject data= dataAr.getJSONObject(i);
                            chatList.add(new ChatModel(data));
                        }
                        adapter.notifyDataSetChanged();
                    }

                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_TOKEN) );
                logManager.logD(TAG, params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }
}

