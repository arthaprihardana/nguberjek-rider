package com.driver.nguberjek;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.NguberjekRiderDialog;
import com.driver.nguberjek.model.OrderDetailModel;
import com.driver.nguberjek.model.SelfieOrderModel;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.PerubahanStatusOrderAPI;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TrackingOrderActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback, RoutingListener {
    private static final String TAG = TrackingOrderActivity.class.getSimpleName();

    OrderDetailModel orderDetailModel;
    SelfieOrderModel selfieOrderModel;

    private ArrayList<Polyline> polylines;
    private static final int[] COLORS = new int[]{R.color.colorPrimary};
    GoogleMap map;
    CountDownTimer countDownPosition;

    ImageView ivKecelakaan, ivLocation;
    Button btnSelesai;

    double Lat, Long;
    boolean afterPause = false;
    String status;

    RelativeLayout btn_direction;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_order);

        setModel();

        setContent();
        setNavigation();

        setCountDownPosition();
    }

    private void setNavigation(){
        btn_direction=(RelativeLayout) findViewById(R.id.btn_direction);
        btn_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.equals("1")) {
                    if (orderDetailModel != null) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Lat + "," + Long);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                }else{
                    if (selfieOrderModel != null) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Lat + "," + Long);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                }
            }
        });
    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        status = b.getString("status");
        if (status.equals("1"))
            orderDetailModel = (OrderDetailModel) b.getSerializable("ORDER_DETAIL");
        else {
            selfieOrderModel = (SelfieOrderModel) b.getSerializable("ORDER_DETAIL");
        }
    }

    public void setContent() {
        ivKecelakaan = (ImageView) findViewById(R.id.iv_kecelakaan);
        ivLocation = (ImageView) findViewById(R.id.iv_location);
        btnSelesai = (Button) findViewById(R.id.btn_selesai);

        ivKecelakaan.setOnClickListener(this);
        ivLocation.setOnClickListener(this);
        btnSelesai.setOnClickListener(this);
    }



    public void setCountDownPosition() {
        /*300000*/
        countDownPosition = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                finish();
                startActivity(getIntent());

                //Do what you want
            }
        };

        setPosition();
    }

    public void setPosition() {
        if (status.equals("1")) {
            Lat = Double.parseDouble(orderDetailModel.latitudeLT);
            Long = Double.parseDouble(orderDetailModel.longitudeLT);
        } else {
            Lat = Double.parseDouble(selfieOrderModel.latitudeLT);
            Long = Double.parseDouble(selfieOrderModel.longitudeLT);
        }

        countDownPosition.start();

        polylines = new ArrayList<>();
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(TrackingOrderActivity.this);
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(final GoogleMap map) {
        this.map = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LatLng start = new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude), end = new LatLng(Lat, Long);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 10));

        drawCorrection(start, end, map);

        map.addMarker(new MarkerOptions()
                .position(end)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.lokasi_awal))
                .title("Location"));

        map.animateCamera(CameraUpdateFactory.zoomTo(15), 5000, null);
    }

    public void drawCorrection(LatLng start, LatLng end, GoogleMap map) {
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
//                .key("AIzaSyCT69Dk7FFoHoPVMCPOcMUPjQE5XLfhSPI")
                .withListener(TrackingOrderActivity.this)
                .alternativeRoutes(false)
                .waypoints(start, end)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        hideDialog();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
        setDialog();
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int j) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(4 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = map.addPolyline(polyOptions);
            polylines.add(polyline);

            int icon = 0;
            if (user.get(SessionManager.KEY_ROLE).equals("nguberjek"))
                icon = R.mipmap.rider;
            else if(user.get(SessionManager.KEY_ROLE).equals("ngubertaxi"))
                icon = R.mipmap.driver_taxi;
            else if(user.get(SessionManager.KEY_ROLE).equals("ngubercar"))
                icon = R.mipmap.driver_car;
            else icon = R.mipmap.rider;

            map.addMarker(new MarkerOptions()
                    .position(new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude))
                    .icon(BitmapDescriptorFactory.fromResource(icon)));

            hideDialog();
            /*logManager.logD(TAG, "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue());*/
        }
    }

    @Override
    public void onRoutingCancelled() {
        hideDialog();
    }

    @Override
    public void onClick(View v) {
        if (v == ivKecelakaan) {
            NguberjekRiderDialog nguberjekRiderDialog = new NguberjekRiderDialog(context, "Apakah anda yakin untuk memberhentikan perjalalan?", new NguberjekRiderDialog.OnDialogConditionListener() {
                @Override
                public void onDialogSelect(boolean positive) {
                    if (positive) {
                        if (status.equals("1")) {
                            PerubahanStatusOrderAPI perubahanStatusOrderAPI = new PerubahanStatusOrderAPI(TrackingOrderActivity.this, "5", orderDetailModel, user);
                        } else {
                            ubahStatusPerjalanan("3");
                        }
                    }
                }
            });
            nguberjekRiderDialog.show();
        } else if (v == ivLocation) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 13));
            map.animateCamera(CameraUpdateFactory.zoomTo(14), 5000, null);
        } else if (v == btnSelesai) {
            if (status.equals("1")) {
                PerubahanStatusOrderAPI perubahanStatusOrderAPI = new PerubahanStatusOrderAPI(TrackingOrderActivity.this, "4", orderDetailModel, user);
            }else
                ubahStatusPerjalanan("2");
        }
    }

    private void ubahStatusPerjalanan(final String s) {
        final String url = AppConstans.APISelfieOrderUpdate;
        String tag_json_obj = "selfie";
        setDialog();

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    logManager.logD(TAG, response.toString());
                    String status = response.getString("s");
                    String message = response.getString("msg");

                    if (status.equals("1")) {
                        if (s.equals("3")) {
                            checkOrder();

                            Log.d(TAG, response.toString());
                        } else {
                            hideDialog();
                            NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

                            Bundle b = new Bundle();
                            b.putString("informasi", "Perjalanan Selesai\nTotal harga perjalanan anda Rp. " + rupiahFormat.format(Double.parseDouble(selfieOrderModel.harga)) + ";");
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtras(b);

                            ActivityManager.getInstance().finishAll();
                            context.startActivity(intent);
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(TrackingOrderActivity.this,
                            e.toString(),
                            Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(TrackingOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    ubahStatusPerjalanan(s);
                    loginAPI.loginAPI = false;
                }else {
                    try {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }catch (Throwable throwable){}
                }

                logManager.logE(TAG, "Error: " + error.getMessage());
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));
                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                Calendar c = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = formatter.format(c.getTime());

                params.put("orderId", selfieOrderModel.orderId);
                params.put("status", s);
                params.put("riderId",user.get(SessionManager.KEY_ID));

                logManager.logD(TAG, params.toString());
                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void checkOrder() {
        String url = AppConstans.APIDataSelfieOrder + selfieOrderModel.orderId;
        String tag_json_obj = "selfie";

        Log.d(TAG, url);

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    Log.d(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();
                        JSONArray data = response.getJSONArray("dt");
                        for(int i = 0; i < data.length(); i++){
                            JSONObject dt = data.getJSONObject(0);

                            selfieOrderModel = new SelfieOrderModel(dt);

                            Log.d(TAG, selfieOrderModel.orderId);
                        }

                        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

                        Bundle b = new Bundle();
                        b.putString("informasi", "Terjadi Hambatan, Perjalanan tidak sampai tujuan. Pembayaran dipotong 50% menjadi Rp. " + rupiahFormat.format(Double.parseDouble(selfieOrderModel.harga)) + ";");
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtras(b);

                        ActivityManager.getInstance().finishAll();
                        context.startActivity(intent);
                    } else {
                        hideDialog();

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    Log.d("Aduh", e.getMessage());

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(TrackingOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    checkOrder();
                    loginAPI.loginAPI = false;
                }else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                Log.d(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    protected void onPause() {
        super.onPause();

        afterPause = true;
        if (countDownPosition != null)
            countDownPosition.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (afterPause) {
            countDownPosition.start();

            map.clear();
            setPosition();
        }
    }
}
