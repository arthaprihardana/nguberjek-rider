package com.driver.nguberjek;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.adapter.StatusTopUpAdapter;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.StatusTopUpModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StatusTopUpActivty extends BaseActivity {
    private final String TAG = RiwayatActivity.class.getSimpleName();

    ArrayList<StatusTopUpModel> statusTopUpModels = new ArrayList<>();

    RecyclerView rvStatusTopUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_top_up_activty);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        setActionBar();
        riwayatTopUp();
        getData();
        setContent();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("TOP UP");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setContent(){
        rvStatusTopUp = (RecyclerView) findViewById(R.id.rv_statusTopUp);

        rvStatusTopUp.setLayoutManager(new LinearLayoutManager(this));
    }

    public void riwayatTopUp(){
        String url = AppConstans.APITopUpDB + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "payment";

        setDialog();

        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "RESPONSE ==> " + response.toString());
                try {
                    int status = response.getInt("s");
                    String msg = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();
                        JSONArray data = response.getJSONArray("dt");

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dt = data.getJSONObject(i);
                            StatusTopUpModel topUpList = new StatusTopUpModel(dt);
                            statusTopUpModels.add(topUpList);
                            getData();
                        }
                    } else {
                        hideDialog();
                        Toast.makeText(StatusTopUpActivty.this, msg, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                Toast.makeText(StatusTopUpActivty.this, "Connection failes", Toast.LENGTH_SHORT).show();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));
                logManager.logD(TAG, params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());
                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

        /**
        StatusTopUpModel statusTopUpModel = new StatusTopUpModel();
        statusTopUpModel.setKode_pembayaran("8608111111112");
        statusTopUpModel.setNominal("100000");
        statusTopUpModel.setStatus("Pending");
        statusTopUpModels.add(statusTopUpModel);

        statusTopUpModel = new StatusTopUpModel();
        statusTopUpModel.setKode_pembayaran("24950811111112");
        statusTopUpModel.setNominal("100000");
        statusTopUpModel.setStatus("Pending");
        statusTopUpModels.add(statusTopUpModel);

        statusTopUpModel = new StatusTopUpModel();
        statusTopUpModel.setKode_pembayaran("1280811111112");
        statusTopUpModel.setNominal("100000");
        statusTopUpModel.setStatus("Pending");
        statusTopUpModels.add(statusTopUpModel);

        statusTopUpModel = new StatusTopUpModel();
        statusTopUpModel.setKode_pembayaran("8760811111112");
        statusTopUpModel.setNominal("100000");
        statusTopUpModel.setStatus("Pending");
        statusTopUpModels.add(statusTopUpModel);

        statusTopUpModel = new StatusTopUpModel();
        statusTopUpModel.setKode_pembayaran("1508111111112");
        statusTopUpModel.setNominal("100000");
        statusTopUpModel.setStatus("Paid");
        statusTopUpModels.add(statusTopUpModel);

        StatusTopUpAdapter statusTopUpAdapter = new StatusTopUpAdapter(StatusTopUpActivty.this, statusTopUpModels);
        rvStatusTopUp.setAdapter(statusTopUpAdapter);

        Toast.makeText(StatusTopUpActivty.this, String.valueOf(statusTopUpAdapter.getItemCount()), Toast.LENGTH_SHORT).show();
         */
    }

    public void getData(){
        RecyclerView rvStatusTopUp = (RecyclerView) findViewById(R.id.rv_statusTopUp);
        RelativeLayout rlNoTopUpList = (RelativeLayout) findViewById(R.id.rl_noTopUpList);

        rvStatusTopUp.setLayoutManager(new LinearLayoutManager(this));

        if(statusTopUpModels.size() != 0) {
            rvStatusTopUp.setVisibility(View.VISIBLE);
            rlNoTopUpList.setVisibility(View.GONE);

            StatusTopUpAdapter statusTopUpAdapter = new StatusTopUpAdapter(StatusTopUpActivty.this, statusTopUpModels);
            rvStatusTopUp.setAdapter(statusTopUpAdapter);

        } else {
            rvStatusTopUp.setVisibility(View.GONE);
            rlNoTopUpList.setVisibility(View.VISIBLE);
        }

    }
}
