package com.driver.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.driver.nguberjek.R;
import com.driver.nguberjek.model.StatusTopUpModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Codelabs on 19/08/2016.
 */
public class StatusTopUpAdapter extends RecyclerView.Adapter<StatusTopUpAdapter.StatusTopUpViewHolder> {
    Context context;
    ArrayList<StatusTopUpModel> statusTopUpModels;

    public StatusTopUpAdapter(Context context, ArrayList<StatusTopUpModel> statusTopUpModels) {
        this.context = context;
        this.statusTopUpModels = statusTopUpModels;
    }

    public StatusTopUpViewHolder onCreateViewHolder(ViewGroup parent, final int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_status_top_up, parent, false);

        return new StatusTopUpViewHolder(v);
    }

    public static class StatusTopUpViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvKodePembayaran, tvNominal, tvStatus;

        public StatusTopUpViewHolder(View v) {
            super(v);

            tvKodePembayaran = (TextView) v.findViewById(R.id.tv_idStatusTopUp);
            tvNominal = (TextView) v.findViewById(R.id.tv_nomoinalStatusTopUp);
            tvStatus = (TextView) v.findViewById(R.id.tv_status);
        }

        public void onClick(View v) {

        }

        public static interface ViewHolderClick {
            public void onClick(View v);
        }
    }

    @Override
    public void onBindViewHolder(StatusTopUpViewHolder holder, int i) {
        final StatusTopUpModel statusTopUpModel = statusTopUpModels.get(i);

        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

        holder.tvKodePembayaran.setText(statusTopUpModel.paymentCode);
        holder.tvNominal.setText("Nominal: Rp. " + rupiahFormat.format(Double.parseDouble(statusTopUpModel.grossAmount)));

        if(statusTopUpModel.transactionStatus.toString().toLowerCase().equals("pending")) {
            holder.tvStatus.setBackgroundResource(R.drawable.corner_orange);
        } else {
            holder.tvStatus.setBackgroundResource(R.drawable.corner_blue);
        }
        holder.tvStatus.setText(statusTopUpModel.transactionStatus);

        /**
        holder.tvKodePembayaran.setText("Kode Pembayaran: " + statusTopUpModel.getKode_pembayaran());
        holder.tvNominal.setText("Nominal: Rp. " + rupiahFormat.format(Double.parseDouble(statusTopUpModel.getNominal())));

        if (statusTopUpModel.getStatus().toString().toLowerCase().equals("pending")) {
            holder.tvStatus.setBackgroundResource(R.drawable.corner_orange);
        }else{
            holder.tvStatus.setBackgroundResource(R.drawable.corner_blue);
        }
        holder.tvStatus.setText(statusTopUpModel.getStatus());
         */
    }

    @Override
    public int getItemCount() {
        return statusTopUpModels.size();
    }
}
