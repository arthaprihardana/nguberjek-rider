package com.driver.nguberjek.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.BantuanActivity;
import com.driver.nguberjek.HistoryActivity;
import com.driver.nguberjek.OrderListActivity;
import com.driver.nguberjek.R;
import com.driver.nguberjek.TentangKamiActivity;
import com.driver.nguberjek.TopUpActivity;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.NguberjekRiderDialog;
import com.driver.nguberjek.model.NavigationDrawerModel;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LogManager;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

/**
 * Created by Nur If Alan on 22/04/2016.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.NavigationDrawerViewHolder> {
    private final String TAG = NavigationDrawerAdapter.class.getSimpleName();
    LogManager logManager = new LogManager();
    Context mContext;

    SessionManager sessionManager;
    HashMap<String, String> user;

    ArrayList<NavigationDrawerModel> navigationDrawerModels;

    SpotsDialog dialog;

    public NavigationDrawerAdapter(Context context, ArrayList<NavigationDrawerModel> navigationDrawerModels) {
        this.mContext = context;
        this.navigationDrawerModels = navigationDrawerModels;

        this.sessionManager = new SessionManager(context);
        this.user = sessionManager.getUserDetails();
    }

    public NavigationDrawerViewHolder onCreateViewHolder(ViewGroup parent, final int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_navdraw, parent, false);

        return new NavigationDrawerViewHolder(v);
    }

    public static class NavigationDrawerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout menu;
        TextView tvMenu;
        ImageView ivLogo;

        public NavigationDrawerViewHolder(View v) {
            super(v);
            tvMenu = (TextView) v.findViewById(R.id.tv_menu);
            ivLogo = (ImageView) v.findViewById(R.id.iv_logo);
            menu = (LinearLayout) v.findViewById(R.id.menu);

            menu.setOnClickListener(this);
        }

        public void onClick(View v) {

        }

        public static interface ViewHolderClick {
            public void onClick(View v);
        }
    }

    public void onBindViewHolder(NavigationDrawerViewHolder holder, final int i) {
        final NavigationDrawerModel navigationDrawerModel = navigationDrawerModels.get(i);


        View rootView = ((Activity) mContext).getWindow().getDecorView().findViewById(android.R.id.content);
        final DrawerLayout drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);

        if (navigationDrawerModel.getMenu() == 1) {
            holder.ivLogo.setImageResource(R.mipmap.histori);
            holder.tvMenu.setText("Riwayat");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    v.getContext().startActivity(new Intent(v.getContext(), HistoryActivity.class));
                }
            });
        } else if (navigationDrawerModel.getMenu() == 2) {
            holder.ivLogo.setImageResource(R.mipmap.top_up);
            holder.tvMenu.setText("Top up");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    v.getContext().startActivity(new Intent(v.getContext(), TopUpActivity.class));
                }
            });
        } else if (navigationDrawerModel.getMenu() == 3) {
            holder.ivLogo.setImageResource(R.mipmap.pelanggan);
            holder.tvMenu.setText("Order");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    v.getContext().startActivity(new Intent(v.getContext(), OrderListActivity.class));
                }
            });
        } else if (navigationDrawerModel.getMenu() == 4) {
            holder.ivLogo.setImageResource(R.mipmap.bantuan);
            holder.tvMenu.setText("Bantuan");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    v.getContext().startActivity(new Intent(v.getContext(), BantuanActivity.class));
                }
            });
        } else if (navigationDrawerModel.getMenu() == 5) {
            holder.ivLogo.setImageResource(R.mipmap.book);
            holder.tvMenu.setText("Panduan");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    try {
                        Uri uri = Uri.parse("http://nguberjek.com/panduan-rider-dan-driver-nguberjek"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        mContext.startActivity(intent);
                    }catch (Throwable throwable){}


                   /* Uri uri = Uri.parse("market://details?id=com.panduan.nguberjek");
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        v.getContext().startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        v.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=com.panduan.nguberjek")));
                    }*/
                }
            });
        } else if (navigationDrawerModel.getMenu() == 6) {
            holder.ivLogo.setVisibility(View.GONE);
            holder.tvMenu.setText("Tentang Kami");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    v.getContext().startActivity(new Intent(v.getContext(), TentangKamiActivity.class));
                }
            });
        } else if (navigationDrawerModel.getMenu() ==7) {
            holder.ivLogo.setVisibility(View.GONE);
            holder.tvMenu.setText("Keluar");

            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    NguberjekRiderDialog nguberjekRiderDialog = new NguberjekRiderDialog(v.getContext(), "Apakah anda yakin ingin keluar?", new NguberjekRiderDialog.OnDialogConditionListener() {
                        @Override
                        public void onDialogSelect(boolean positive) {
                            if (positive) {
                                drawer.closeDrawer(GravityCompat.START);

                                logout(v.getContext());
                            }
                        }
                    });
                    nguberjekRiderDialog.show();

//                    AlertDialog.Builder builder=new AlertDialog.Builder(v.getContext());
//                    builder.setTitle("Apakah anda yakin ingin keluar?");
//                    builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            drawer.closeDrawer(GravityCompat.START);
//
//                            logout(v.getContext());
//                        }
//                    });
//                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.cancel();
//                        }
//                    });
//                    builder.setCancelable(false);
//                    builder.show();
                }
            });
        }
    }

    public int getItemCount() {
        return navigationDrawerModels.size();
    }

    public void logout(final Context context) {
        String url = AppConstans.APILogout + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "json_obj_req";

        dialog = new SpotsDialog(context, R.style.Custom);
        dialog.show();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");
                    int statusCode = response.getInt("statusCode");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hidePDialog();

                        sessionManager.logoutUser();
                        ActivityManager.getInstance().finishAll();
                    } else {
                        hidePDialog();
                        /*paxPartnerDialog = new PaxPartnerDialog(context, status, message, new PaxPartnerDialog.OnDialogConditionListener() {
                            @Override
                            public void onDialogSelect(boolean positive) {
                            }
                        });
                        paxPartnerDialog.show();*/
                        View rootView = ((Activity) mContext).getWindow().getDecorView().findViewById(android.R.id.content);

                        Snackbar snackbar = Snackbar
                                .make((CoordinatorLayout) rootView.findViewById(R.id
                                        .coordinatorlayout), message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hidePDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(context, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if (loginAPI.loginAPI) {
                    logout(context);
                    loginAPI.loginAPI = false;
                } else {
                    View rootView = ((Activity) mContext).getWindow().getDecorView().findViewById(android.R.id.content);

                    Snackbar snackbar = Snackbar
                            .make((CoordinatorLayout) rootView.findViewById(R.id
                                    .coordinatorlayout), R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }

                VolleyLog.d(TAG, "Error: " + error.getMessage());
                logManager.logD(TAG, "Error: " + user.get(SessionManager.KEY_ID));
                hidePDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }*/
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void hidePDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
