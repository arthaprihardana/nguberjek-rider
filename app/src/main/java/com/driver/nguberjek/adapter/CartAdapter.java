package com.driver.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.driver.nguberjek.R;
import com.driver.nguberjek.model.AddressModel;
import com.driver.nguberjek.model.CartModel;
import com.driver.nguberjek.model.MenuModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by adikurniawan on 03/12/17.
 */

public class CartAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    Context context;
    ArrayList<MenuModel> rowList=new ArrayList<MenuModel>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    CartModel cartModel;


    public CartAdapter(Context context, ArrayList<MenuModel> rowList, CartModel cartModel){
        this.context=context;
        this.rowList=rowList;
        this.cartModel=cartModel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart, viewGroup, false);

            return new CartAdapter.ViewHolder(itemView);
        } else if (i == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart_header, viewGroup, false);

            return new CartAdapter.ViewHolderHeader(itemView);
        }else if(i==TYPE_FOOTER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart_footer, viewGroup, false);

            return new CartAdapter.ViewHolderFooter(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMenuName;
        TextView tvMenuPrice;
        TextView tvQty,tvMinus, tvPlus;
        TextView tvNote;
        EditText etNote;


        ViewHolder(View itemView) {
            super(itemView);
            tvMenuName=(TextView)itemView.findViewById(R.id.tvMenuName);
            tvMenuPrice=(TextView)itemView.findViewById(R.id.tvMenuPrice);
            tvQty=(TextView)itemView.findViewById(R.id.tvQty);
            tvMinus=(TextView)itemView.findViewById(R.id.tvMinus);
            tvPlus=(TextView)itemView.findViewById(R.id.tvPlus);
            tvNote=(TextView)itemView.findViewById(R.id.tvAddNote);
            etNote=(EditText)itemView.findViewById(R.id.etNote);

        }
    }

    static class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView tvRestoName, tvRestoAddress;

        ViewHolderHeader(View itemView) {
            super(itemView);
            tvRestoName=(TextView)itemView.findViewById(R.id.tvRestoName);
            tvRestoAddress=(TextView)itemView.findViewById(R.id.tvAddress);
        }
    }

    static class ViewHolderFooter extends RecyclerView.ViewHolder {
        TextView tvAddMore;
        TextView btnLocation;
        TextView tvAddNote;
        EditText etNote;
        TextView tvAddress;

        // payment details //
        TextView tvLabelDelivery;
        TextView tvPriceFood, tvPriceDelivery, tvPriceTotal;

        ViewHolderFooter(View itemView) {
            super(itemView);

            btnLocation=(TextView) itemView.findViewById(R.id.tvChange);
            tvAddNote=(TextView)itemView.findViewById(R.id.tvAddNote);
            etNote=(EditText) itemView.findViewById(R.id.etNote);
            tvAddress=(TextView)itemView.findViewById(R.id.tvAddress);

            tvLabelDelivery=(TextView)itemView.findViewById(R.id.tvLabelDelivery);
            tvPriceFood=(TextView)itemView.findViewById(R.id.tvPriceFood);
            tvPriceDelivery=(TextView)itemView.findViewById(R.id.tvPriceDelivery);
            tvPriceTotal=(TextView)itemView.findViewById(R.id.tvTotalPrice);


        }
    }

    @Override
    public int getItemCount() {
        return rowList.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
            return TYPE_HEADER;
        else if(position==rowList.size()+1)
            return TYPE_FOOTER;
        else
            return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder variableViewHolder,final int i) {

        if (variableViewHolder instanceof CartAdapter.ViewHolder) {
            final CartAdapter.ViewHolder holder = (CartAdapter.ViewHolder) variableViewHolder;
            final MenuModel data=rowList.get(i-1);

            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

            holder.tvMenuName.setText(data.name);
            holder.tvQty.setText(data.qty+"");
            int subTotal=data.qty*data.price;
            holder.tvMenuPrice.setText(""+formatRupiah.format(subTotal));

            if (holder.etNote != null)
                holder.etNote.setText(data.note);
            holder.etNote.addTextChangedListener(new EditTextWatcher(data));


            holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            holder.tvNote.setVisibility(View.VISIBLE);
            holder.etNote.setVisibility(View.GONE);

            holder.tvNote.setText(data.note);




        } else if (variableViewHolder instanceof CartAdapter.ViewHolderHeader) {
            CartAdapter.ViewHolderHeader holder = (CartAdapter.ViewHolderHeader) variableViewHolder;
            holder.tvRestoName.setText(cartModel.restonName);
            holder.tvRestoAddress.setText(cartModel.restoAddress);



        } else if(variableViewHolder instanceof  CartAdapter.ViewHolderFooter){
            final CartAdapter.ViewHolderFooter holder =(CartAdapter.ViewHolderFooter) variableViewHolder;


            holder.btnLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            holder.tvAddNote.setVisibility(View.VISIBLE);
            holder.etNote.setVisibility(View.GONE);

            holder.tvAddNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tvAddNote.setVisibility(View.GONE);
                    holder.etNote.setVisibility(View.VISIBLE);
                    holder.etNote.requestFocus();
                }
            });


            holder.tvAddress.setText(cartModel.alamatPengantaran);


            /// payment details ///

            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);


            holder.tvPriceFood.setText(formatRupiah.format(cartModel.subTotal));
            holder.tvLabelDelivery.setText("Delivery ("+cartModel.jarak+")");
            holder.tvPriceDelivery.setText(formatRupiah.format(cartModel.biayaAntar));
            holder.tvPriceTotal.setText(formatRupiah.format(cartModel.total));

        }

    }


    public class EditTextWatcher implements TextWatcher {

        private MenuModel data;

        public EditTextWatcher(MenuModel data) {
            this.data = data;
        }

        @Override
        public void afterTextChanged(Editable s) {
            data.note=s.toString();
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

    }

    public class EditTextWatcherAddress implements TextWatcher {

        private AddressModel data;

        public EditTextWatcherAddress(AddressModel data) {
            this.data = data;
        }

        @Override
        public void afterTextChanged(Editable s) {
            data.note=s.toString();
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

    }



}