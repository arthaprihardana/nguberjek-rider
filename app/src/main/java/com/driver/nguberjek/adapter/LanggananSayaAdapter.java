package com.driver.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.driver.nguberjek.R;
import com.driver.nguberjek.model.LanggananSayaModel;

import java.util.ArrayList;

/**
 * Created by Codelabs on 16/08/2016.
 */
public class LanggananSayaAdapter extends RecyclerView.Adapter<LanggananSayaAdapter.LanggananSayaViewHolder>{
    Context context;
    ArrayList<LanggananSayaModel> langgananSayaModels;

    public LanggananSayaAdapter(Context context, ArrayList<LanggananSayaModel> langgananSayaModels){
        this.context = context;
        this.langgananSayaModels = langgananSayaModels;
    }

    public LanggananSayaViewHolder onCreateViewHolder(ViewGroup parent, final int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_langganan_saya, parent, false);

        return new LanggananSayaViewHolder(v);
    }

    public static class LanggananSayaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNamaPelanggan, tvNoTelepon;

        public LanggananSayaViewHolder(View v) {
            super(v);

            tvNamaPelanggan =(TextView) v.findViewById(R.id.tv_namaPelanggan);
            tvNoTelepon = (TextView) v.findViewById(R.id.tv_noPelanggan);
        }

        public void onClick(View v) {

        }

        public static interface ViewHolderClick {
            public void onClick(View v);
        }
    }

    @Override
    public void onBindViewHolder(LanggananSayaAdapter.LanggananSayaViewHolder holder, int i) {
        final LanggananSayaModel langgananSayaModel = langgananSayaModels.get(i);

        holder.tvNamaPelanggan.setText(langgananSayaModel.getNama_pelanggan());
        holder.tvNoTelepon.setText(langgananSayaModel.getNo_pelanggan());
    }

    @Override
    public int getItemCount() {
        return langgananSayaModels.size();
    }
}
