package com.driver.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.driver.nguberjek.R;
import com.driver.nguberjek.model.RiwayatSelfieOrderModel;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sragen on 9/27/2016.
 */
public class RiwayatSelfieOrderAdapter extends RecyclerView.Adapter<RiwayatSelfieOrderAdapter.RiwayatSelfieViewHolder>{
    Context context;
    ArrayList<RiwayatSelfieOrderModel> riwayatModels;

    public RiwayatSelfieOrderAdapter(Context context, ArrayList<RiwayatSelfieOrderModel> riwayatModels){
        this.context = context;
        this.riwayatModels = riwayatModels;
    }

    public RiwayatSelfieViewHolder onCreateViewHolder(ViewGroup parent, final int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_riwayat, parent, false);

        return new RiwayatSelfieViewHolder(v);
    }

    public static class RiwayatSelfieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvWaktu, tvLokasiAwal, tvTujuan, tvJarak, tvHarga;

        public RiwayatSelfieViewHolder(View v) {
            super(v);

            tvWaktu =(TextView) v.findViewById(R.id.tv_waktu);
            tvLokasiAwal = (TextView) v.findViewById(R.id.tv_lokasi);
            tvTujuan = (TextView) v.findViewById(R.id.tv_tujuan);
            tvJarak = (TextView) v.findViewById(R.id.tv_jarak);
            tvHarga = (TextView) v.findViewById(R.id.tv_harga);
        }

        public void onClick(View v) {

        }

        public static interface ViewHolderClick {
            public void onClick(View v);
        }
    }

    public void onBindViewHolder(RiwayatSelfieViewHolder holder, int i) {
        final RiwayatSelfieOrderModel riwayatModel = riwayatModels.get(i);

        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

        holder.tvWaktu.setText(customConvertDate(riwayatModel.tanggal));
        holder.tvLokasiAwal.setText(riwayatModel.lokasi_awal);
        holder.tvTujuan.setText(riwayatModel.lokasi_akhir);
        holder.tvJarak.setText(riwayatModel.jarak  + " KM");
        holder.tvHarga.setText("Rp. " + rupiahFormat.format(Double.parseDouble(riwayatModel.harga)));
    }

    public String customConvertDate(String currentDate) {
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy, hh:mm a");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    @Override
    public int getItemCount() {
        return riwayatModels.size();
    }
}
