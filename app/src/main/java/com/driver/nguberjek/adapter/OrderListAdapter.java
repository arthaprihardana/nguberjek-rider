package com.driver.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.driver.nguberjek.R;
import com.driver.nguberjek.model.OrderListModel;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by masihsayang on 9/3/2016.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.RiwayatViewHolder> {
    Context context;
    ArrayList<OrderListModel> orderListModels;

    public OrderListAdapter(Context context, ArrayList<OrderListModel> orderListModels) {
        this.context = context;
        this.orderListModels = orderListModels;
    }

    public RiwayatViewHolder onCreateViewHolder(ViewGroup parent, final int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_list, parent, false);

        return new RiwayatViewHolder(v);
    }

    public static class RiwayatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNama, tvJarak, tvHarga, tvLokasiAwal, tvLokasiAkhir;
        Button btnTolak, btnTerima;

        public RiwayatViewHolder(View v) {
            super(v);

            tvNama = (TextView) v.findViewById(R.id.tv_nama);
            tvJarak = (TextView) v.findViewById(R.id.tv_jarak);
            tvHarga = (TextView) v.findViewById(R.id.tv_harga);
            tvLokasiAwal = (TextView) v.findViewById(R.id.tv_lokasi);
            tvLokasiAkhir = (TextView) v.findViewById(R.id.tv_tujuan);

            btnTolak = (Button) v.findViewById(R.id.btn_tolak);
            btnTerima = (Button) v.findViewById(R.id.btn_terima);
        }

        public void onClick(View v) {

        }

        public static interface ViewHolderClick {
            public void onClick(View v);
        }
    }

    @Override
    public void onBindViewHolder(RiwayatViewHolder holder, int i) {
        final OrderListModel orderListModel = orderListModels.get(i);

        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

        holder.tvNama.setText(orderListModel.getNama());
        holder.tvJarak.setText(orderListModel.getJarak());
        holder.tvHarga.setText(orderListModel.getHarga());
        holder.tvLokasiAwal.setText(orderListModel.lokasi_awal + " KM");
        holder.tvLokasiAkhir.setText(orderListModel.lokasi_akhir);
    }

    public String customConvertDate(String currentDate) {
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy, hh:mm a");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    @Override
    public int getItemCount() {
        return orderListModels.size();
    }
}