package com.driver.nguberjek;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.driver.nguberjek.pushnotification.AudioPlayer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.driver.nguberjek.adapter.NavigationDrawerAdapter;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.dialog.NguberjekRiderDialog;
import com.driver.nguberjek.model.NavigationDrawerModel;
import com.driver.nguberjek.model.OrderDetailModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.PerubahanStatusOrderAPI;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderViewActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnMapReadyCallback, RoutingListener {
    private final String TAG = OrderViewActivity.class.getSimpleName();

    OrderDetailModel orderDetailModel;

    private ArrayList<Polyline> polylines;
    private static final int[] COLORS = new int[]{R.color.colorPrimary};
    GoogleMap map;

    CountDownTimer countDownPosition;

    ImageView ivOk, ivCancel, ivTelp, ivChat, ivEditProfile;
    TextView tvJarak;
    RelativeLayout btn_info_food;

    double Lat, Long;
    boolean afterPause = false;
    String orderId;
    RelativeLayout btn_direction;

    private boolean isFood=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view);

        setModel();

        setNavigationDrawer();

        menuNavigationDrawer();
        setNavigation();

        checkOrder();

    }

    private void setNavigation(){
        btn_direction=(RelativeLayout) findViewById(R.id.btn_direction);
        btn_info_food=(RelativeLayout) findViewById(R.id.btn_info_food);
        btn_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(orderDetailModel!=null) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Lat + "," + Long);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });
        if(isFood) btn_info_food.setVisibility(View.VISIBLE);
        else btn_info_food.setVisibility(View.GONE);

        btn_info_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("orderId",orderId);
                Intent intent=new Intent(OrderViewActivity.this, OrderFoodActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        orderId = b.getString("orderId");

        logManager.logD(TAG,orderId);
        if(orderId.contains("food_"))isFood=true;



        ///// OPEN CHAT ACTIVITY /////
        /*AppConstans.firstTimeChat=true;
        Intent i=new Intent(OrderViewActivity.this, ChatActivity.class);
        i.putExtra("orderId", orderId);
        startActivity(i);*/
        /////===================/////
    }

    public void setNavigationDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        ImageView ivStar1 = (ImageView) headerView.findViewById(R.id.iv_star1);
        ImageView ivStar2 = (ImageView) headerView.findViewById(R.id.iv_star2);
        ImageView ivStar3 = (ImageView) headerView.findViewById(R.id.iv_star3);
        ImageView ivStar4 = (ImageView) headerView.findViewById(R.id.iv_star4);
        ImageView ivStar5 = (ImageView) headerView.findViewById(R.id.iv_star5);
        ivEditProfile = (ImageView) headerView.findViewById(R.id.iv_editProfil);
        TextView tvNamaLengkap = (TextView) headerView.findViewById(R.id.tv_namaLengkap);
        TextView tvNomorTelepon = (TextView) headerView.findViewById(R.id.tv_nomorTelepon);
        TextView tvPlat = (TextView) headerView.findViewById(R.id.tv_plat);

        if (user.get(SessionManager.KEY_RATING).trim().equals("1")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star_line);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("2")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("3")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("4")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star);
            ivStar5.setImageResource(R.mipmap.star_line);
        } else if (user.get(SessionManager.KEY_RATING).trim().equals("5")) {
            ivStar1.setImageResource(R.mipmap.star);
            ivStar2.setImageResource(R.mipmap.star);
            ivStar3.setImageResource(R.mipmap.star);
            ivStar4.setImageResource(R.mipmap.star);
            ivStar5.setImageResource(R.mipmap.star);
        } else {
            ivStar1.setImageResource(R.mipmap.star_line);
            ivStar2.setImageResource(R.mipmap.star_line);
            ivStar3.setImageResource(R.mipmap.star_line);
            ivStar4.setImageResource(R.mipmap.star_line);
            ivStar5.setImageResource(R.mipmap.star_line);
        }

        tvNamaLengkap.setText(user.get(SessionManager.KEY_NAMA));
        tvNomorTelepon.setText(user.get(SessionManager.KEY_NO_TELP));
        tvPlat.setText(user.get(SessionManager.KEY_NO_POLISI));
        ivEditProfile.setOnClickListener(this);
    }

    public void menuNavigationDrawer() {
        RecyclerView mRecyclerView, mRecyclerView2;

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_menuNd);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<NavigationDrawerModel> list = new ArrayList<NavigationDrawerModel>();
        NavigationDrawerModel navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(1);

        list.add(navigationDrawerModel);

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(2);

        list.add(navigationDrawerModel);

        /*navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(3);

        list.add(navigationDrawerModel);*/

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(4);

        list.add(navigationDrawerModel);

        NavigationDrawerAdapter navDrawAdapter = new NavigationDrawerAdapter(OrderViewActivity.this, list);
        mRecyclerView.setAdapter(navDrawAdapter);

        mRecyclerView2 = (RecyclerView) findViewById(R.id.rv_menuNd2);
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<NavigationDrawerModel>();

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(5);

        list.add(navigationDrawerModel);

        navigationDrawerModel = new NavigationDrawerModel();
        navigationDrawerModel.setMenu(6);

        list.add(navigationDrawerModel);

        NavigationDrawerAdapter navDrawAdapter2 = new NavigationDrawerAdapter(OrderViewActivity.this, list);
        mRecyclerView2.setAdapter(navDrawAdapter2);
    }

    public void checkOrder() {
        String url = AppConstans.APIOrderDetail + orderId;
        String tag_json_obj = "tempOrder";

        Log.d(TAG, url);

        setDialog();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    Log.d(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();
                        JSONArray dataArray= response.getJSONArray("dt");
                        if(dataArray.length()>0){
                            JSONObject data = dataArray.getJSONObject(0);
                            if(isFood){
                                orderDetailModel = new OrderDetailModel(data,true);
                            }else{
                                orderDetailModel = new OrderDetailModel(data);
                            }

                            setContent();
                        }
                    } else {
                        hideDialog();

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(OrderViewActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if (loginAPI.loginAPI) {
                    checkOrder();
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                Log.d(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void setContent() {
        TextView tvNamaPelanggan = (TextView) findViewById(R.id.tv_namaPelanggan);
        TextView tvHarga = (TextView) findViewById(R.id.tv_harga);
        TextView tvDari = (TextView) findViewById(R.id.tv_dari);
        TextView tvKe = (TextView) findViewById(R.id.tv_ke);
        tvJarak = (TextView) findViewById(R.id.tv_jarak);

        ivTelp = (ImageView) findViewById(R.id.iv_telepon);
        ivOk = (ImageView) findViewById(R.id.iv_ok);
        ivCancel = (ImageView) findViewById(R.id.iv_cancel);
        ivChat = (ImageView) findViewById(R.id.iv_chat);

        tvNamaPelanggan.setText(orderDetailModel.nama);
        tvHarga.setText("Rp. " + rupiahFormat.format(Double.parseDouble(orderDetailModel.harga)) + ";");
        tvDari.setText(orderDetailModel.lokasiAwal);
        tvKe.setText(orderDetailModel.lokasiTujuan);
        tvJarak.setText(orderDetailModel.jarak + " Km");

        setCountDownPosition();

        ivOk.setOnClickListener(this);
        ivCancel.setOnClickListener(this);
        ivTelp.setOnClickListener(this);
        ivChat.setOnClickListener(this);
    }

    public void setCountDownPosition() {
        /*300000*/
        countDownPosition = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                map.clear();
                setPosition();

                //Do what you want
            }
        };

        setPosition();
    }

    public void setPosition() {
        Lat = Double.parseDouble(orderDetailModel.latitudeLA);
        Long = Double.parseDouble(orderDetailModel.longitudeLA);

        countDownPosition.start();

        polylines = new ArrayList<>();
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(OrderViewActivity.this);
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(final GoogleMap map) {
        this.map = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LatLng start = new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude), end = new LatLng(Lat, Long);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 15));

        drawCorrection(start, end, map);

        map.addMarker(new MarkerOptions()
                .position(end)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.lokasi_awal))
                .title("Location"));
    }

    public void drawCorrection(LatLng start, LatLng end, GoogleMap map) {
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(OrderViewActivity.this)
                .alternativeRoutes(false)
                .waypoints(start, end)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        hideDialog();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
        setDialog();
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int j) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(4 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = map.addPolyline(polyOptions);
            polylines.add(polyline);

            int icon = 0;
            if (user.get(SessionManager.KEY_ROLE).equals("nguberjek"))
                icon = R.mipmap.rider;
            else if(user.get(SessionManager.KEY_ROLE).equals("ngubertaxi"))
                icon = R.mipmap.driver_taxi;
            else if(user.get(SessionManager.KEY_ROLE).equals("ngubercar"))
                icon = R.mipmap.driver_car;
            else icon = R.mipmap.rider;

            map.addMarker(new MarkerOptions()
                    .position(new LatLng(getCurrentLocation().latitude, getCurrentLocation().longitude))
                    .icon(BitmapDescriptorFactory.fromResource(icon)));

            afterPause = true;
            hideDialog();
            /*logManager.logD(TAG, "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue());*/
        }
    }

    @Override
    public void onRoutingCancelled() {
        hideDialog();
    }

    @Override
    public void onClick(View v) {
        if (v == ivEditProfile) {
            startActivity(new Intent(OrderViewActivity.this, SuntingProfileActivity.class));
        } else if (v == ivOk) {
            PerubahanStatusOrderAPI perubahanStatusOrderAPI = new PerubahanStatusOrderAPI(OrderViewActivity.this, "3", orderDetailModel, user);
        } else if (v == ivCancel) {
            NguberjekRiderDialog nguberjekRiderDialog = new NguberjekRiderDialog(context, "Apakah anda yakin untuk membatalkan orderan?", new NguberjekRiderDialog.OnDialogConditionListener() {
                @Override
                public void onDialogSelect(boolean positive) {
                    if (positive) {
                        PerubahanStatusOrderAPI perubahanStatusOrderAPI = new PerubahanStatusOrderAPI(OrderViewActivity.this, "7", orderDetailModel, user);
                    }
                }
            });
            nguberjekRiderDialog.show();
        } else if (v == ivTelp) {

            NguberjekRiderDialog nguberjekRiderDialog = new NguberjekRiderDialog(context, "Apakah anda yakin untuk melakukan panggilan?", new NguberjekRiderDialog.OnDialogConditionListener() {
                @Override
                public void onDialogSelect(boolean positive) {
                    if (positive) {
                        callingPermission();
                    }
                }
            });
            nguberjekRiderDialog.show();
        } else if (v == ivChat) {

            /*NguberjekRiderDialog nguberjekRiderDialog = new NguberjekRiderDialog(context, "Apakah anda yakin untuk melakukan panggilan?", new NguberjekRiderDialog.OnDialogConditionListener() {
                @Override
                public void onDialogSelect(boolean positive) {
                    if (positive) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address", orderDetailModel.noTelp);
                        startActivity(smsIntent);
                    }
                }
            });
            nguberjekRiderDialog.show();*/

            Intent i=new Intent(OrderViewActivity.this, ChatActivity.class);
            i.putExtra("orderId", orderId);
            i.putExtra("telepon", orderDetailModel.noTelp);
            startActivity(i);
        }
    }

    final private int REQUEST_CODE_CALLING_PERMISSION = 1;

    private void callingPermission() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_CALLING_PERMISSION);
            return;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + orderDetailModel.noTelp));
        if (ActivityCompat.checkSelfPermission(OrderViewActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        afterPause = false;

        if (countDownPosition != null)
            countDownPosition.cancel();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (countDownPosition != null)
            countDownPosition.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (afterPause) {
            countDownPosition.start();

            map.clear();
            setPosition();
        }
    }

    @Override
    public void onBackPressed() {

    }
}
