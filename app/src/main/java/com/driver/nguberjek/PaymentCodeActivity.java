package com.driver.nguberjek;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.driver.nguberjek.model.Code128;
import com.driver.nguberjek.utilities.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentCodeActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = TopUpActivity.class.getSimpleName();

    Button btnKembali;
    String responseVeritrans;
    TextView tv_kodePembayaran, tv_kodePembayaran2, tv_descNominal;
    ImageView iv_barcodeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_code);

        setActionBar();
        setModel();
        setContent();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("TOP UP");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        responseVeritrans = b.getString("responseVeritrans");
    }

    public void setContent(){
        btnKembali = (Button) findViewById(R.id.btn_kembali);
        tv_kodePembayaran = (TextView) findViewById(R.id.tv_kodePembayaran);
        tv_descNominal = (TextView) findViewById(R.id.tv_desc_nominal);
        iv_barcodeView = (ImageView) findViewById(R.id.iv_barcodeView);

        try {
            JSONObject dt = new JSONObject(responseVeritrans);
            String paymentCode = dt.getString("paymentCode");
            drawBarcode(paymentCode.toString());

            Log.i(TAG, "Payment Code ==> " + paymentCode);
            tv_kodePembayaran.setText(paymentCode);
            tv_descNominal.setText("Silahkan anda melakukan pembayaran ke indomaret sebesar Rp. " + rupiahFormat.format(Double.parseDouble(dt.getString("grossAmount"))) + "; dengan nomor kode pembayaran");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        btnKembali.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnKembali){
            finish();
        }
    }

    private void drawBarcode(String paymentCode) {
        String barcode = paymentCode.toString();
        Code128 code = new Code128(this);
        code.setData(barcode);
        Bitmap bitmap = code.getBitmap(680, 300);
        ImageView ivBarcode = (ImageView)findViewById(R.id.iv_barcodeView);
        ivBarcode.setImageBitmap(bitmap);
    }
}
