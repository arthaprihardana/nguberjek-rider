package com.driver.nguberjek;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.driver.nguberjek.adapter.CartAdapter;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.AddressModel;
import com.driver.nguberjek.model.CartModel;
import com.driver.nguberjek.model.MenuModel;
import com.driver.nguberjek.model.OrderDetailModel;
import com.driver.nguberjek.model.RestoModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adikurniawan on 03/12/17.
 */

public class OrderFoodActivity extends BaseActivity {

    private ArrayList<MenuModel> menuKeranjang=new ArrayList<MenuModel>();
    private RecyclerView recyclerView;
    private CartAdapter adapter;
    private RestoModel dataResto;
    private CartModel dataOrder;
    private String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_order);



        getBundle();
        setContent();
        setNavigation();
        setToolBar();
        get_detail_order_food();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getBundle(){
        Bundle b=getIntent().getExtras();
        orderId=b.getString("orderId");
    }

    private void setContent(){
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    private void setData(){
        adapter=new CartAdapter(this, dataOrder.menuList, dataOrder);
        recyclerView.setAdapter(adapter);
    }


    private void setNavigation(){
    }



    public void get_detail_order_food() {
        orderId=orderId.replace("food_","");
        String url = AppConstans.APIOrderDetailFood + orderId;
        String tag_json_obj = "tempOrder";
        Log.d("url",url);

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("response",response.toString());
                    int status = response.getInt("s");
                    String message = response.getString("msg");
                    if (status == 1) {
                        JSONArray dataArray = response.getJSONArray("dt");
                        if(dataArray.length()>0) {
                            JSONObject data = dataArray.getJSONObject(0);
                            dataOrder = new CartModel(data);

                            setData();
                        }
                    } else {
                        Toast.makeText(OrderFoodActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("response", e.getMessage());

                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(OrderFoodActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);
                if (loginAPI.loginAPI) {
                    get_detail_order_food();
                    loginAPI.loginAPI = false;
                } else {
                    Toast.makeText(OrderFoodActivity.this, R.string.fail_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }







    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Daftar Pesanan");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
