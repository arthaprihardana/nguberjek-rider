package com.driver.nguberjek;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class TentangKamiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang_kami);

        setActionBar();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Tentang Kami");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void btnInstagram(View view){
        /*Uri uri = Uri.parse("https://www.instagram.com/nguberjek_idn/");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        *//*try {*//*
            startActivity(likeIng);*//*
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/nguberjek_idn/")));
        }*/

        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.instagram.com/nguberjek")));
    }

    public void btnFacebook(View view){
        /*Uri uri = Uri.parse("https://www.instagram.com/nguberjek_idn/");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        *//*try {*//*
            startActivity(likeIng);*//*
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/nguberjek_idn/")));
        }*/

        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/nguberjek")));
    }

    public void btnTwitter(View view){
        /*Uri uri = Uri.parse("https://www.instagram.com/nguberjek_idn/");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        *//*try {*//*
            startActivity(likeIng);*//*
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/nguberjek_idn/")));
        }*/

        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://twitter.com/nguberjek")));
    }

    public void btnWebsite(View view){
        /*Uri uri = Uri.parse("https://www.instagram.com/nguberjek_idn/");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        *//*try {*//*
            startActivity(likeIng);*//*
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/nguberjek_idn/")));
        }*/

        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.nguberjek.com")));
    }

    public void btnRating(View view){
        /*Uri uri = Uri.parse("https://www.instagram.com/nguberjek_idn/");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        *//*try {*//*
            startActivity(likeIng);*//*
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/nguberjek_idn/")));
        }*/

        Uri uri = Uri.parse("market://details?id=com.nguberjek.boncenger");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            view.getContext().startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.nguberjek.boncenger")));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
