package com.driver.nguberjek.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.driver.nguberjek.R;

/**
 * Created by adikurniawan on 11/25/16.
 */
@SuppressLint("DefaultLocale")
public class DialogCancelOrder extends Dialog implements View.OnClickListener {

    TextView tv1, tv2, tv3, tv4, tv5;
    ListenerDialog listenerDialog;

    public DialogCancelOrder(Context context, ListenerDialog listener){
        super(context, R.style.TransparantDialogTheme);
        this.listenerDialog=listener;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_cancel_order);

        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        tv3=(TextView)findViewById(R.id.tv3);
        tv4=(TextView)findViewById(R.id.tv4);
        tv5=(TextView)findViewById(R.id.tv5);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);

    }

    public interface ListenerDialog{
        void onSelected(boolean isi,String value);
    }

    @Override
    public void onClick(View v) {
        if(v==tv1){
            dismiss();
            listenerDialog.onSelected(true,tv1.getText().toString());
        }else if(v==tv2){
            dismiss();
            listenerDialog.onSelected(true,tv2.getText().toString());
        }else if(v==tv3){
            dismiss();
            listenerDialog.onSelected(true,tv3.getText().toString());
        }else if(v==tv4){
            dismiss();
            listenerDialog.onSelected(true,tv4.getText().toString());
        }else if(v==tv5){
            dismiss();
            listenerDialog.onSelected(false,tv5.getText().toString());
        }
    }

}