package com.driver.nguberjek.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.driver.nguberjek.R;

/**
 * Created by if_alan on 9/13/2016.
 */
public class NguberjekRiderDialog extends Dialog implements View.OnClickListener{
    TextView tvMessage;
    View btnYa, btnTidak;

    private OnDialogConditionListener onCancelListener;

    public NguberjekRiderDialog(Context context, String m, OnDialogConditionListener onCancelListener) {
        super(context);
        this.onCancelListener = onCancelListener;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        setContentView(R.layout.nguberjek_rider_dialog);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        btnYa = findViewById(R.id.btn_ya);
        btnTidak = findViewById(R.id.btn_tidak);

        tvMessage.setText(m);

        btnYa.setOnClickListener(this);
        btnTidak.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnYa){
            dismiss();
            onCancelListener.onDialogSelect(true);
        }else{
            dismiss();
        }
    }

    public interface OnDialogConditionListener {
        void onDialogSelect(boolean positive);
    }
}