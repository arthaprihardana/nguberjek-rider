package com.driver.nguberjek.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.driver.nguberjek.R;

/**
 * Created by masihsayang on 9/13/2016.
 */
public class InformasiDialog extends Dialog{
    TextView tvMessage;
    View btnYa, btnTidak;

    public InformasiDialog(Context context, String m) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        setContentView(R.layout.informasi_dialog);
        setCancelable(true);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        LinearLayout informasiDialog = (LinearLayout) findViewById(R.id.informasi_dialog);

        tvMessage.setText(m);
        informasiDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}