package com.driver.nguberjek;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.BaseActivity;

public class SplashscreenActivity extends BaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = null;

                if (sessionManager.isLoggedIn()) {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    ActivityManager.getInstance().finishAll();
                } else {
                    mainIntent = new Intent(SplashscreenActivity.this,
                            MainActivity.class);
                    startActivity(mainIntent);
                    ActivityManager.getInstance().finishAll();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
