package com.driver.nguberjek;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.driver.nguberjek.model.Session;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.LogManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.SessionManager;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;
import dmax.dialog.SpotsDialog;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class SuntingProfileActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = SuntingProfileActivity.class.getSimpleName();

    private Uri finalImage;
    private Uri mCropImageUri;

    ImageView ivProfile;
    EditText etNama, etNoTelepon;
    Button btnSimpan;

    private String image_link_uploaded="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunting_profile);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        setActionBar();

        setContent();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Sunting Profil");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setContent() {
        ivProfile = (ImageView) findViewById(R.id.iv_profilPicture);
        etNama = (EditText) findViewById(R.id.et_nama);
        etNoTelepon = (EditText) findViewById(R.id.et_noTelepon);

        image_link_uploaded=user.get(SessionManager.KEY_PHOTO);
        Glide.with(this)
                .load(user.get(SessionManager.KEY_PHOTO))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .override(150,150)
                .into(ivProfile);

        etNama.setText(user.get(SessionManager.KEY_EMAIL));
        etNoTelepon.setText(user.get(SessionManager.KEY_NO_TELP));

        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        btnSimpan.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
    }

    public void updateDataRider(final String nama, final String noTelepon) {
        String url = AppConstans.APIUpdateDataRider + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "change_password";

        setDialog();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    hideDialog();
                    if (status == 1) {

                        Toast.makeText(SuntingProfileActivity.this, message.toLowerCase(), Toast.LENGTH_SHORT).show();
                        finish();
                        login(user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD));
                    } else {

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(SuntingProfileActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    updateDataRider(nama, noTelepon);
                    loginAPI.loginAPI = false;
                }else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                logManager.logD(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (nama.length() != 0) {
                    params.put("email", nama);
                }

                if (noTelepon.length() != 0) {
                    params.put("noTelp", noTelepon);
                }

                if (image_link_uploaded.length() != 0) {
                    params.put("photo", image_link_uploaded);
                }

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    public void login(final String username, final String password) {
        String url = AppConstans.APILogin;
        String tag_json_obj = "json_obj_req";

        if (!((Activity) context).isFinishing()) {
            dialog.show();
        }

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        hideDialog();

                        sessionManager.editor.clear();
                        sessionManager.editor.commit();

                        //int statusCode = response.getInt("statusCode");
                        //int expired = response.getInt("expired");

                        JSONObject data = response.getJSONObject("dt");

                        Session sessionModel = new Session();
                        sessionModel.set_id(data.getString("_id"));
                        sessionModel.setLatitude(data.getString("latitude"));
                        sessionModel.setLongitude(data.getString("longitude"));
                        sessionModel.setSenderId(data.getString("senderId"));
                        sessionModel.setUsername(data.getString("username"));
                        sessionModel.setLokasi(data.getString("lokasi"));
                        sessionModel.setNoPolisi(data.getString("noPolisi"));
                        sessionModel.setMerkKendaraan(data.getString("merkKendaraan"));
                        sessionModel.setTypeKendaraan(data.getString("typeKendaraan"));
                        sessionModel.setJenisKelamin(data.getString("jenisKelamin"));
                        sessionModel.setRole(data.getString("role"));
                        sessionModel.setNoTelp(data.getString("noTelp"));
                        sessionModel.setEmail(data.getString("email"));
                        sessionModel.setNama(data.getString("nama"));
                        sessionModel.setToken(response.getString("token"));
                        sessionModel.setSaldo(data.getString("saldo"));
                        sessionModel.setReady(data.getString("ready"));
                        sessionModel.setShow(data.getString("show"));
                        sessionModel.setFranchise(data.getString("franchise"));
                        sessionModel.setPassword(password);
                        sessionModel.setPhoto(data.getString("photo"));

                        if(!data.isNull("franchiseName")) sessionModel.setFranchiseName(data.getString("franchiseName"));
                        else sessionModel.setFranchiseName("franchiseName");

                        if(!data.isNull("rating")) sessionModel.setRating(data.getString("rating"));
                        else sessionModel.setRating("5");



                        sessionManager.createLoginSession(sessionModel);

                        finish();
                    } else {
                        hideDialog();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

                logManager.logE(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("password", password);
                params.put("senderId", AppConstans.token_gcm);

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void uploadFoto() {
        final SpotsDialog[] uploadPhotoDialog = new SpotsDialog[1];

        File myFile = new File(finalImage.getPath());
        RequestParams params = new RequestParams();
        try {
            params.put("userImage", myFile);
        } catch(FileNotFoundException e) {}

        RestClient.post("uploadImage",params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                uploadPhotoDialog[0] =new SpotsDialog(SuntingProfileActivity.this, R.style.UploadPhoto);
                uploadPhotoDialog[0].setCancelable(false);
                uploadPhotoDialog[0].show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                //Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_SHORT).show();
                try {
                    Log.d("result",response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    if(status.equals("1")){
                        JSONObject data =response.getJSONObject("dt");
                        image_link_uploaded=data.getString("link");

                        updateDataRider(etNama.getText().toString(), etNoTelepon.getText().toString());
                    }else{
                        Toast.makeText(SuntingProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
                uploadPhotoDialog[0].dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                uploadPhotoDialog[0].dismiss();
                try {
                    JSONObject data=new JSONObject(responseString);
                    Log.d("response",responseString);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
                //Log.d("result",responseString.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    public static class RestClient {
        private static final String BASE_URL = AppConstans.APIUpload;
//        private static final String BASE_URL = "http://159.203.109.20:8080/";
        private static AsyncHttpClient client = new AsyncHttpClient();
        public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(BASE_URL, params, responseHandler);
        }
        public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.post(BASE_URL, params, responseHandler);
        }
        private static String getAbsoluteUrl(String relativeUrl) {
            Log.d("url",BASE_URL+relativeUrl);
            return BASE_URL + relativeUrl;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnSimpan) {
            if (etNama.getText().toString().trim().length() != 0 && etNoTelepon.getText().toString().trim().length() != 0) {
                if (isEmailValid(etNama.getText().toString())) {
                    if (finalImage == null) {
                        // no need for upload foto //
                        updateDataRider(etNama.getText().toString(), etNoTelepon.getText().toString());
                    } else {
                        // need upload foto //
                        uploadFoto();
                    }
                } else {
                    etNama.setError("Email is not valid");
                }
            } else {
                if (etNama.getText().toString().trim().length() == 0)
                    etNama.setError("This Field is Required");
                else
                    etNoTelepon.setError("This Field is Required");
            }
        }else if(v == ivProfile){
            CropImage.startPickImageActivity(this);
        }
    }

    public boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                finalImage=result.getUri();
                ((ImageView) findViewById(R.id.iv_profilPicture)).setImageURI(result.getUri());
                Glide.with(this)
                        .load(result.getUri())
                        .bitmapTransform(new CropCircleTransformation(this))
                        .into(ivProfile);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();

                Log.d(TAG, "Cropping failed: " + result.getError());
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                //.setMultiTouchEnabled(true)
                .start(this);
    }
}
