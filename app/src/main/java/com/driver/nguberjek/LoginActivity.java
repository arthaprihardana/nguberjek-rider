package com.driver.nguberjek;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.pushnotification.RegistrationIntentService;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.api.LoginAPI;

/**
 * Created by Nur If Alan on 29/08/2016.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = LoginActivity.class.getSimpleName();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

//    SessionManager sessionManager;
//    List<Session> sessionList = new ArrayList<Session>();

//    CoordinatorLayout coordinatorLayout;
    TextView btnForgotPassword;
    Button btnMasuk;
    EditText etUsername, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (checkPlayServices()) {
            Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
            startService(intent);
        }

        setActionBar();
        setContent();

//        sessionManager = new SessionManager(getApplicationContext());
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Masuk");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setContent() {
        AppConstans.orderID = "";

//        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
//                .coordinatorlayout);
        btnForgotPassword = (TextView) findViewById(R.id.btn_forgotPassword);
        btnMasuk = (Button) findViewById(R.id.btn_masuk);
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);

        /*etUsername.setText("if_alan@ymail.com");
        etPassword.setText("37750");*/

        btnForgotPassword.setOnClickListener(this);
        btnMasuk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnForgotPassword) {
            startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
        } else if (v == btnMasuk) {
            hideSoftKeyboard(LoginActivity.this);

            if (etUsername.getText().toString().trim().length() > 0 && etPassword.getText().toString().length() > 0) {
                LoginAPI loginAPI = new LoginAPI(LoginActivity.this, etUsername.getText().toString(), etPassword.getText().toString(), true);
                //login(etUsername.getText().toString(), etPassword.getText().toString());
            } else {
                if (etUsername.getText().toString().trim().length() == 0)
                    etUsername.setError("This Field is Required");
                else
                    etPassword.setError("This Field is Required");
            }
        }
    }

//    public void login(final String username, final String password) {
//        String url = AppConstans.APILogin;
//        String tag_json_obj = "json_obj_req";
//
//        setDialog();
//
//        // creating request obj
//        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                try {
//                    int status = response.getInt("s");
//                    String message = response.getString("msg");
//
//                    logManager.logD(TAG, response.toString());
//
//                    if (status == 1) {
//                        hideDialog();
//
//                        int statusCode = response.getInt("statusCode");
//                        int expired = response.getInt("expired");
//
//                        JSONObject data = response.getJSONObject("dt");
//
//                        Session sessionModel = new Session();
//                        sessionModel.set_id(data.getString("_id"));
//                        sessionModel.setUsername(data.getString("username"));
//                        sessionModel.setNoPolisi(data.getString("noPolisi"));
//                        sessionModel.setNoTelp(data.getString("noTelp"));
//                        sessionModel.setEmail(data.getString("email"));
//                        sessionModel.setNama(data.getString("nama"));
//                        sessionModel.setToken(response.getString("token"));
//                        sessionModel.setPhoto(data.getString("photo"));
//                        sessionModel.setPassword(password);
//                        sessionList.add(sessionModel);
//
//                        sessionManager.createLoginSession(sessionList);
//
//                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                    } else {
//                        hideDialog();
//
//                        Snackbar snackbar = Snackbar
//                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
//
//                        View sbView = snackbar.getView();
//                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                        textView.setGravity(Gravity.CENTER_HORIZONTAL);
//
//                        snackbar.show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),
//                            "Error: " + e.getMessage(),
//                            Toast.LENGTH_LONG).show();
//
//                    hideDialog();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Snackbar snackbar = Snackbar
//                        .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
//                snackbar.show();
//
//                logManager.logE(TAG, "Error: " + error.getMessage());
//                hideDialog();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                return params;
//            }
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//
//                params.put("username", username);
//                params.put("password", password);
//                params.put("senderId", AppConstans.token_gcm);
//
//                logManager.logD(TAG, params.toString());
//
//                return params;
//            }
//        };
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                R.integer.MY_SOCKET_TIMEOUT_MS,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
//    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}
