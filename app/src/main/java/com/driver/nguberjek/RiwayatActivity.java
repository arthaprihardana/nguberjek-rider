package com.driver.nguberjek;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.adapter.RiwayatAdapter;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.RiwayatModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RiwayatActivity extends BaseActivity {
    private final String TAG = RiwayatActivity.class.getSimpleName();

    ArrayList<RiwayatModel> riwayatModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        setActionBar();
        takeRiwayatData();
        getData();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Riwayat");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void takeRiwayatData() {
        String url = AppConstans.APIRiwayat + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "history";

        setDialog();

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String msg = response.getString("msg");

                    logManager.logD(TAG, response.toString());

                    hideDialog();

                    if (status == 1) {
                        JSONArray data = response.getJSONArray("dt");

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dt = data.getJSONObject(i);

                            RiwayatModel riwayatModel = new RiwayatModel(dt);

                            riwayatModels.add(riwayatModel);

                            getData();
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

                LoginAPI loginAPI = new LoginAPI(RiwayatActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);


                if (loginAPI.loginAPI) {
                    takeRiwayatData();
                    loginAPI.loginAPI = false;
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void getData() {
        RecyclerView rvRiwayat = (RecyclerView) findViewById(R.id.rv_riwayat);
        RelativeLayout rlNoRiwayatData = (RelativeLayout) findViewById(R.id.rl_noRiwayatData);

        rvRiwayat.setLayoutManager(new LinearLayoutManager(this));

        if (riwayatModels.size() != 0) {
            rvRiwayat.setVisibility(View.VISIBLE);
            rlNoRiwayatData.setVisibility(View.GONE);

            RiwayatAdapter riwayatAdapter = new RiwayatAdapter(RiwayatActivity.this, riwayatModels);
            rvRiwayat.setAdapter(riwayatAdapter);
        } else {
            rvRiwayat.setVisibility(View.GONE);
            rlNoRiwayatData.setVisibility(View.VISIBLE);
        }
    }
}
