package com.driver.nguberjek;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//import com.driver.nguberjek.utilities.LoginAPI;

// public class TopUpActivity extends AppCompatActivity implements View.OnClickListener {
public class TopUpActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = TopUpActivity.class.getSimpleName();

    Button btnStatusTopUp;
    TextView tv_saldo, tvTopUp;
    ListView topup_price_list;

    String KEY_TEXTPSS = "TEXTPSS";
    static final int CUSTOM_DIALOG_ID = 0;

    String[] listContent = {"Rp. 50.000;", "Rp. 100.000;", "Rp. 300.000;", "Rp. 500.000;"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);

        sessionManager = new SessionManager(context);
        user = sessionManager.getUserDetails();

        setActionBar();
        setContent();
        getSaldo();
    }

    public void setContent(){
        btnStatusTopUp = (Button) findViewById(R.id.btn_statusTopUp);
        tvTopUp = (TextView) findViewById(R.id.tv_topUp) ;
        tv_saldo = (TextView) findViewById(R.id.tv_saldo);

        btnStatusTopUp.setOnClickListener(this);
        // tvTopUp.setOnClickListener(this);

        tvTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(CUSTOM_DIALOG_ID);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case CUSTOM_DIALOG_ID:
                dialog = new Dialog(TopUpActivity.this);
                dialog.setContentView(R.layout.item_topup_list);
                dialog.setTitle("Pilih Nominal :");
                topup_price_list = (ListView) dialog.findViewById(R.id.list_topup_price);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listContent);
                topup_price_list.setAdapter(adapter);
                topup_price_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(id == 0) {
                            Log.i(TAG, "SALDO Rp. 50.000");
                            chargeTopUp("50000");
                        } else if(id == 1) {
                            Log.i(TAG, "SALDO Rp. 100.000");
                            chargeTopUp("100000");
                        } else if(id == 2) {
                            Log.i(TAG, "SALDO Rp. 300.000");
                            chargeTopUp("300000");
                        } else if (id == 3) {
                            Log.i(TAG, "SALDO Rp. 500.000");
                            chargeTopUp("500000");
                        }
                        dismissDialog(CUSTOM_DIALOG_ID);
                    }
                });
            break;
        }
        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
        // TODO Auto-generated method stub
        super.onPrepareDialog(id, dialog, bundle);

        switch(id) {
            case CUSTOM_DIALOG_ID:
                break;
        }
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("TOP UP");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v == btnStatusTopUp){
            startActivity(new Intent(TopUpActivity.this, StatusTopUpActivty.class));
        } /** else if(v == tvTopUp){
            // startActivity(new Intent(TopUpActivity.this, PaymentCodeActivity.class));
            // chargeTopUp("100000");
            // chargeTopUp("50000");
            Log.i(TAG, "click menu pilhan topup");
        } */
    }

    public void getSaldo() {
        Log.e(TAG,"USER KEY ID ==> " + user.get(SessionManager.KEY_ID));

        String url = AppConstans.APISisaSaldo + user.get(SessionManager.KEY_ID);
        String tag_json_obj = "saldo";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");

                    // logManager.logD(TAG, response.toString());

                    if (status == 1) {
                        int statusCode = response.getInt("statusCode");
                        String dt = String.valueOf(response.getString("dt"));

                        if (response.getInt("dt") != 0) {
                            tv_saldo.setText("Rp. " + rupiahFormat.format(response.getLong("dt")) + ";");
                        }else {
                            tv_saldo.setText("Rp. 0.000;");
                        }
                    } else {
                        Toast.makeText(TopUpActivity.this, message, Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "message ==> " + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    // hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                LoginAPI loginAPI = new LoginAPI(TopUpActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                logManager.logD(TAG, params.toString());

                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void chargeTopUp(final String tvTopUp) {
        String url = AppConstans.APITopUpCharge;
        final String riderId = user.get(SessionManager.KEY_ID);
        String tag_json_obj = "topup";

        setDialog();

        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int status = response.getInt("s");
                    String message = response.getString("msg");
                    Log.i(TAG, "==> response ==> " + response.toString());

                    if(status == 1) {
                        hideDialog();
                        String responseVeritrans = response.getString("body").toString();
                        Intent intent = new Intent(TopUpActivity.this, PaymentCodeActivity.class);
                        Bundle b = new Bundle();
                        b.putString("responseVeritrans", responseVeritrans);
                        intent.putExtras(b);
                        startActivity(intent);
                        // finish();
                    } else {
                        hideDialog();
                    }

                } catch (JSONException e) {
                    e.getStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Log.i(TAG, "Token ==> " + user.get(SessionManager.KEY_TOKEN));
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("riderId", riderId);
                params.put("amount", tvTopUp);

                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

//    public Dialog showDialog() {
//        Log.i(TAG, "muncul show dialog");
//        AlertDialog.Builder builder = new AlertDialog.Builder(TopUpActivity.this);
//        builder.setTitle(R.string.topuptitle)
//                .setItems(R.array.topuppricelist, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        Log.i(TAG, "Top Up yang dipilih ==> " + which);
//                    }
//                });
//        return builder.create();
//    }
}
