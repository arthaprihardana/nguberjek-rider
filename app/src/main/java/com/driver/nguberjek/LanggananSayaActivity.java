package com.driver.nguberjek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.driver.nguberjek.adapter.LanggananSayaAdapter;
import com.driver.nguberjek.model.LanggananSayaModel;

import java.util.ArrayList;

public class LanggananSayaActivity extends AppCompatActivity {
    ArrayList<LanggananSayaModel> langgananSayaModels = new ArrayList<>();

    RecyclerView rvLangananSaya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_langganan_saya);

        setActionBar();
        setContent();
        getData();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Langganan Saya");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void setContent(){
        rvLangananSaya = (RecyclerView) findViewById(R.id.rv_langgananSaya);

        rvLangananSaya.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getData(){
        LanggananSayaModel langgananSayaModel = new LanggananSayaModel();
        langgananSayaModel.setNama_pelanggan("Iman Nur Arifin");
        langgananSayaModel.setNo_pelanggan("08999999999");
        langgananSayaModels.add(langgananSayaModel);

        langgananSayaModel = new LanggananSayaModel();
        langgananSayaModel.setNama_pelanggan("Prika Dewi");
        langgananSayaModel.setNo_pelanggan("0877877777");
        langgananSayaModels.add(langgananSayaModel);

        LanggananSayaAdapter langgananSayaAdapter = new LanggananSayaAdapter(LanggananSayaActivity.this, langgananSayaModels);
        rvLangananSaya.setAdapter(langgananSayaAdapter);
    }
}
