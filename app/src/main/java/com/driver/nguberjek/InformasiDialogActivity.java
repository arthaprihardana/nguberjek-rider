package com.driver.nguberjek;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.driver.nguberjek.utilities.BaseActivity;

public class InformasiDialogActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_informasi_dialog);

        setModel();
    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        String message = b.getString("informasi");

        setContent(message);
    }

    public void setContent(String m){
        TextView tvMessage = (TextView) findViewById(R.id.tv_message);
        LinearLayout informasiDialog = (LinearLayout) findViewById(R.id.informasi_dialog);

        tvMessage.setText(m);
        informasiDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
