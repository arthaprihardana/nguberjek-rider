package com.driver.nguberjek;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.driver.nguberjek.app.SharedPreference;
import com.driver.nguberjek.fragment.AutoCompleteDestination;
import com.driver.nguberjek.fragment.AutoCompletePickUp;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.model.SelfieOrderModel;
import com.driver.nguberjek.utilities.AppController;
import com.driver.nguberjek.utilities.BaseActivity;
import com.driver.nguberjek.utilities.CustomRequest;
import com.driver.nguberjek.utilities.LocationAddress;
import com.driver.nguberjek.api.LoginAPI;
import com.driver.nguberjek.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MapSelfieOrderActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, DirectionCallback,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {
    private static final String TAG = MapSelfieOrderActivity.class.getSimpleName();

    ArrayList resultId = null;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    // private static final String API_KEY = "AIzaSyCbj0WG88GvLFjyxNTYBpXGvaO9y_AK6U0"; // API KEY DEVELOPMENT
    private static final String API_KEY = "AIzaSyDnTce5NZQrxVHh6r7hLkAdeTF9VU8Adz4";    // API KEY PRODUCTION

    // maps //
    private GoogleMap map;
    private static LatLng CURRENT_lOCATION;
    private static LatLng DESTINATION_lOCATION;
    private static LatLng TEMP_lOCATION;
    private double latitude, longitude;
    private String currentAddress = "";

    ImageView ivPenumpang, ivLocation;
    RelativeLayout rlMarker;
    AutoCompleteTextView actLokasiAwal, actLokasiTujuan;
    TextView tvOrder, tvHarga, tvJarak, tvWaktu;

    String[] dataPelanggan;
    private boolean isLocationAwal = true;
    private boolean getLocation = true;

    private String service_status = "nguberjek";
    private String distance, duration, price;

    private CountDownTimer timer;
    UiSettings mUiSettings;
    RelativeLayout btn_direction;

    private boolean isFirstTime = true;
    private boolean findLocation = true;


    // autocomplete //
    AutoCompletePickUp placeAutoComplete_pickUp;
    AutoCompleteDestination placeAutoComplete_destination;
    private String pickUp_address;
    private String destination_address;
    private GoogleApiClient mGoogleApiClient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_selfie_order);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorlayout);

        setActionBar();

        setModel();

        setContent();
        setNavigation();

        setSearchBox();

        CURRENT_lOCATION = getCurrentLocation();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("ID")
                .build();

        placeAutoComplete_pickUp = (AutoCompletePickUp) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_pickup);
        placeAutoComplete_destination = (AutoCompleteDestination) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_destination);
        placeAutoComplete_pickUp.setFilter(typeFilter);
        placeAutoComplete_destination.setFilter(typeFilter);
        placeAutoComplete_pickUp.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Log.d("Maps", "Place selected: " + place.getName());

                isLocationAwal = true;
                ivPenumpang.setVisibility(View.VISIBLE);
                rlMarker.setVisibility(View.VISIBLE);

                pickUp_address=place.getAddress().toString();
                CURRENT_lOCATION = place.getLatLng();
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 13));
                map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });

        placeAutoComplete_destination.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Log.d("Maps", "Place selected: " + place.getName());
                isLocationAwal = false;
                findLocation=true;
                ivPenumpang.setVisibility(View.VISIBLE);
                rlMarker.setVisibility(View.VISIBLE);



                destination_address=place.getAddress().toString();
                DESTINATION_lOCATION = place.getLatLng();
                //Log.d("REQUEST DIRECTION","DARI GET LAT LNG");
                //requestDirection();
            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });
    }

    private void setNavigation(){

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG,bundle.toString());

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,i+"");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG,connectionResult.toString());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);

        setMap();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Selfie Order");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void setModel() {
        Bundle b = getIntent().getExtras();
        dataPelanggan = b.getStringArray("dataPelanggan");
    }

    public void setContent() {
        tvOrder = (TextView) findViewById(R.id.tv_order);
        ivLocation = (ImageView) findViewById(R.id.iv_location);
        rlMarker = (RelativeLayout) findViewById(R.id.rl_marker);
        ivPenumpang = (ImageView) findViewById(R.id.iv_penumpang);
        tvHarga = (TextView) findViewById(R.id.tv_harga);
        tvJarak = (TextView) findViewById(R.id.tv_jarak);
        tvWaktu = (TextView) findViewById(R.id.tv_waktu);

//        rlMarker.setVisibility(View.GONE);

        rlMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlMarker.setVisibility(View.GONE);
                ivPenumpang.setVisibility(View.GONE);
                /*final LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(TEMP_lOCATION.latitude, TEMP_lOCATION.longitude,
                        MapSelfieOrderActivity.this.getApplicationContext(), new GeocoderHandler());*/
                getGeoLocation(TEMP_lOCATION.latitude,TEMP_lOCATION.longitude);

            }
        });

        tvOrder.setOnClickListener(this);
        ivLocation.setOnClickListener(this);
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            Double lat, lng;
            String laporanMap = null;
            String final_address = "";

            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    lat = bundle.getDouble("lat");
                    lng = bundle.getDouble("lng");
                    final_address = bundle.getString("address");
                    laporanMap = bundle.getString("message");

                    //locationAddress = bundle.getString("address");
                    break;
                default:
                    lat = 0.0;
                    lng = 0.0;
            }

            if (laporanMap != null) {
                Toast.makeText(MapSelfieOrderActivity.this, laporanMap, Toast.LENGTH_SHORT).show();
            } else {
                latitude = lat;
                longitude = lng;
                currentAddress = final_address;
                if(isLocationAwal) {
                    CURRENT_lOCATION = new LatLng(lat, lng);
                    map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));

                    actLokasiAwal.setText(currentAddress);
                    if(!actLokasiTujuan.getText().toString().trim().equals(""))
                        requestDirection();
                }else{
                    DESTINATION_lOCATION = new LatLng(lat, lng);
                    map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));


                    actLokasiTujuan.setText(currentAddress);
                    if(!actLokasiAwal.getText().toString().trim().equals(""))
                        requestDirection();
                }
                getLocation = false;
            }
        }
    }

    public void requestDirection() {
        hideSoftKeyboard(MapSelfieOrderActivity.this);

        if(user.get(SessionManager.KEY_ROLE).equals("nguberjek")){
            GoogleDirection.withServerKey(API_KEY)
                    .from(CURRENT_lOCATION)
                    .to(DESTINATION_lOCATION)
                    .transportMode(TransportMode.DRIVING)
                    .avoid(AvoidType.TOLLS)
                    .execute(this);
        }else{
            GoogleDirection.withServerKey(API_KEY)
                    .from(CURRENT_lOCATION)
                    .to(DESTINATION_lOCATION)
                    .transportMode(TransportMode.DRIVING)
                    .execute(this);
        }

    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {

            // disable all status //
            findLocation = false;
            rlMarker.setVisibility(View.GONE);
            ivPenumpang.setVisibility(View.GONE);
            ////////////////////////


            map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.mipmap.lokasi_awal)));
            map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.mipmap.lokasi_tujuan)));

            // get distance  and  duration//
            distance = direction.getRouteList().get(0).getLegList().get(0).getDistance().getText();
            duration = direction.getRouteList().get(0).getLegList().get(0).getDuration().getText();

            /*try{
                distance = distance.replace("km", "");
                distance = distance.replace(" ","");
            }catch (Throwable th){}*/
            tvJarak.setText("Jarak : " + distance);
            tvWaktu.setText("Waktu : " + duration);
            getFare();
            actLokasiAwal.clearFocus();
            actLokasiTujuan.clearFocus();

            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.RED));
            LatLngBounds.Builder BOUNDS_CAMERA = new LatLngBounds.Builder();
            BOUNDS_CAMERA.include(DESTINATION_lOCATION).include(CURRENT_lOCATION);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(BOUNDS_CAMERA.build(), 100));


        } else {
            Toast.makeText(MapSelfieOrderActivity.this, "direction not ok", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.d("direction", t.getMessage());
    }

    private void getFare() {
        try {
            distance = distance.replace("km", "");
            distance = distance.replace("m", "");
            distance = distance.replace(" ", "");
        } catch (Throwable th) {
        }


        final String url = AppConstans.APIFare + "?jarak=" + distance
                + "&role=" + user.get(SessionManager.KEY_ROLE);
        Log.d("url",url);
        String tag_json_obj = "getFare";
        //setDialog();

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("s");
                    String message = response.getString("msg");

                    //hideDialog();
                    if (status.equals("1")) {
//                        String pricee = response.getString("price");
                        String pricee = response.getString("dt");

                        tvHarga.setText("Harga : Rp. " + rupiahFormat.format(Double.parseDouble(pricee)) + ";");
                        ivPenumpang.setVisibility(View.GONE);
                        rlMarker.setVisibility(View.GONE);

                        price = pricee;
                    }else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MapSelfieOrderActivity.this,
                            e.toString(),
                            Toast.LENGTH_LONG).show();
                    hideDialog();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoginAPI loginAPI = new LoginAPI(MapSelfieOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    getFare();
                    loginAPI.loginAPI = false;
                }else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                //hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void setSearchBox() {
        actLokasiAwal = (AutoCompleteTextView) findViewById(R.id.act_lokasiAwal);
        actLokasiAwal.setAdapter(new GooglePlacesAutocompleteAdapter(MapSelfieOrderActivity.this, R.layout.item_search_address, R.id.tvAddress));
        actLokasiAwal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("place_id", resultId.get(position).toString());
                getLat(resultId.get(position).toString(), "awal");

                map.clear();
                isLocationAwal = true;
                ivPenumpang.setVisibility(View.VISIBLE);
                rlMarker.setVisibility(View.VISIBLE);

                // Check if no view has focus:
                hideSoftKeyboard(MapSelfieOrderActivity.this);
            }
        });

        actLokasiTujuan = (AutoCompleteTextView) findViewById(R.id.act_lokasiTujuan);
        actLokasiTujuan.setAdapter(new GooglePlacesAutocompleteAdapter(MapSelfieOrderActivity.this, R.layout.item_search_address, R.id.tvAddress));
        actLokasiTujuan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("place_id", resultId.get(position).toString());
                getLat(resultId.get(position).toString(), "tujuan");

                map.clear();
                isLocationAwal = false;
                ivPenumpang.setVisibility(View.VISIBLE);
                rlMarker.setVisibility(View.VISIBLE);

                // Check if no view has focus:
                hideSoftKeyboard(MapSelfieOrderActivity.this);
            }
        });

        actLokasiAwal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isLocationAwal = true;
                    actLokasiAwal.setText("");
                    map.clear();
                    tvHarga.setText("Harga : ");
                    tvJarak.setText("Jarak : ");
                    tvWaktu.setText("Waktu : ");

                    ivPenumpang.setVisibility(View.VISIBLE);
                    rlMarker.setVisibility(View.VISIBLE);
                }
            }
        });

        actLokasiTujuan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isLocationAwal = false;
                    actLokasiTujuan.setText("");
                    map.clear();
                    tvHarga.setText("Harga : ");
                    tvJarak.setText("Jarak : ");
                    tvWaktu.setText("Waktu : ");

                    ivPenumpang.setVisibility(View.VISIBLE);
                    rlMarker.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            String result = "";
            try {
                result = resultList.get(index).toString();
            } catch (Throwable th) {
            }
            return result;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }


        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        resultId = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:id");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            Log.d("url", sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.d(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.d(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            Log.d("result", jsonObj.toString());

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            resultId = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                resultId.add(predsJsonArray.getJSONObject(i).getString("place_id"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }
        return resultList;
    }

    private void setMap() {
       /* try {
            map = ((MapFragment) MapSelfieOrderActivity.this.getFragmentManager().findFragmentById(R.id.map)).getMap();
//            map = ((SupportMapFragment) MapSelfieOrderActivity.this.getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        } catch (Throwable tr) {
        }*/

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 14));
        map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                rlMarker.setVisibility(View.VISIBLE);
            }
        });

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions
                getCurrentLocation();
                return false;
            }
        });

//        final LocationAddress locationAddress = new LocationAddress();

        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                if (findLocation) {
                    // change status for keterangan marker //
                    rlMarker.setVisibility(View.VISIBLE);
                    ivPenumpang.setVisibility(View.VISIBLE);


                }


                TEMP_lOCATION = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
                if (getLocation) {
                    /*locationAddress.getAddressFromLocation(cameraPosition.target.latitude, cameraPosition.target.longitude,
                            MapSelfieOrderActivity.this.getApplicationContext(), new GeocoderHandler());*/
                    getGeoLocation(cameraPosition.target.latitude,cameraPosition.target.longitude);
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    public void getGeoLocation(final double latitude , final double longitude) {
        String sb = "https://maps.googleapis.com/maps/api/geocode/json";
        sb += "?latlng=" +latitude +","+longitude;
        sb += "&key=" + API_KEY;
        Log.d("url geolocation", sb.toString());
        String tag_json_obj = "geolocation";

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, sb, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String formatted_address="";
                    Log.d("output response", response.toString());
                    JSONArray jsonArray=response.getJSONArray("results");
                    if(jsonArray.length()>0){
                        JSONObject data=jsonArray.getJSONObject(0);
                        formatted_address=data.getString("formatted_address");
                    }




                    if (formatted_address.toLowerCase().startsWith("latitude:")) {
                        Toast.makeText(getApplicationContext(),"Maaf koneksi anda terputus, silahkan coba lagi",Toast.LENGTH_LONG);
                    }else {
                        map.clear();
                        String[] address_split = formatted_address.split(",");
                        currentAddress = address_split[0];
                        getLocation = false;

                        if (isFirstTime) {
                            isFirstTime = false;
                            placeAutoComplete_pickUp.setText(currentAddress);
                            pickUp_address=formatted_address;
                        } else {
                            if (isLocationAwal) {
                                CURRENT_lOCATION = new LatLng(latitude, longitude);
                                map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));

                                placeAutoComplete_pickUp.setText(currentAddress);
                                pickUp_address = formatted_address;
                                actLokasiAwal.setText(currentAddress);
                                if (destination_address != null)
                                    requestDirection();
                            } else {
                                DESTINATION_lOCATION = new LatLng(latitude, longitude);
                                map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));


                                placeAutoComplete_destination.setText(currentAddress);
                                destination_address = formatted_address;
                                actLokasiTujuan.setText(currentAddress);
                                if (pickUp_address != null)
                                    requestDirection();
                            }
                        }

                    }















                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "gagal geolocation", Toast.LENGTH_SHORT).show();
                    Log.d("Geolocation", "Error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Maaf koneksi anda terputus, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                VolleyLog.d("get lat", "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    public void getLat(String place_id, final String locationStatus) {
        String sb = "https://maps.googleapis.com/maps/api/place/details/json";
        sb += "?placeid=" + place_id;
        sb += "&key=" + API_KEY;
        Log.d("url detail", sb.toString());
        String tag_json_obj = "search_lat";

        final ProgressDialog dialog = new ProgressDialog(MapSelfieOrderActivity.this);
        dialog.setMessage("loading...");
        dialog.show();

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, sb, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("output response", response.toString());
                    JSONObject result = response.getJSONObject("result");
                    JSONObject geometry = result.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");
                    double latitude = location.getDouble("lat");
                    double longitude = location.getDouble("lng");
                    LatLng LocationKordinat = new LatLng(latitude, longitude);
                    Log.d("kordinat", latitude + " " + longitude);

                    if (locationStatus.equals("awal")) {
                        CURRENT_lOCATION = LocationKordinat;
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 13));
                        map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

                        if(!actLokasiTujuan.getText().toString().trim().equals(""))
                            requestDirection();
                    } else {
                        DESTINATION_lOCATION = LocationKordinat;

                        Log.d(TAG, actLokasiAwal.getText().toString().trim());

                        if(!actLokasiAwal.getText().toString().trim().equals(""))
                            requestDirection();
                    }

                    // request address name lagi //
                    /*final LocationAddress locationAddress = new LocationAddress();
                    locationAddress.getAddressFromLocation(LocationKordinat.latitude, LocationKordinat.longitude,
                            getApplicationContext(), new GeocoderHandler());*/
                    getGeoLocation(LocationKordinat.latitude,LocationKordinat.longitude);
                    hideSoftKeyboard(MapSelfieOrderActivity.this);
                    // end request address name //

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MapSelfieOrderActivity.this,
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("get lat", "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public void onClick(View v) {
        if (v == tvOrder) {
            prepareOrder();
        } else if (v == ivLocation) {
            getCurrentLocation();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 14));
            map.animateCamera(CameraUpdateFactory.zoomTo(14), 3000, null);
        }
    }

    private void prepareOrder() {
        if ((pickUp_address!=null) && (destination_address!=null) && distance != null && duration != null && price != null)
            requestOrder();
        else Toast.makeText(getApplicationContext(),"Data order belum lengkap !",Toast.LENGTH_LONG).show();
    }

    private void requestOrder() {
        final String url = AppConstans.APISelfieOrder;
        String tag_json_obj = "selfie";
        setDialog();

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    hideDialog();
                    logManager.logD(TAG, response.toString());
                    String status = response.getString("s");
                    String message = response.getString("msg");

                    if (status.equals("1")) {
                        JSONObject data = response.getJSONObject("dt");
                        SelfieOrderModel selfieOrderModel = new SelfieOrderModel(data);

                        Intent intent = new Intent(MapSelfieOrderActivity.this, TrackingOrderActivity.class);
                        Bundle b = new Bundle();
                        b.putSerializable("ORDER_DETAIL", selfieOrderModel);
                        b.putString("status", "2");
                        intent.putExtras(b);
                        startActivity(intent);
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MapSelfieOrderActivity.this,
                            e.toString(),
                            Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

                LoginAPI loginAPI = new LoginAPI(MapSelfieOrderActivity.this, user.get(SessionManager.KEY_USERNAME), user.get(SessionManager.KEY_PASSWORD), false);

                if(loginAPI.loginAPI){
                    requestOrder();
                    loginAPI.loginAPI = false;
                }else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.fail_connection, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    snackbar.show();
                }
                Log.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_TOKEN));
                logManager.logD(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                Calendar c = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = formatter.format(c.getTime());

                params.put("tanggal", formattedDate);
                params.put("noTelpKonsumen", dataPelanggan[1]);
                params.put("namaKonsumen", dataPelanggan[0]);
                params.put("riderId", user.get(SessionManager.KEY_ID));
                params.put("lokasiAwal[lokasi]", actLokasiAwal.getText().toString());
                params.put("lokasiAwal[koordinatLokasi]", CURRENT_lOCATION.longitude + "," + CURRENT_lOCATION.latitude);
                params.put("lokasiAkhir[tujuan]", actLokasiTujuan.getText().toString());
                params.put("lokasiAkhir[koordinatTujuan]", DESTINATION_lOCATION.longitude + "," + DESTINATION_lOCATION.latitude);
                params.put("harga", price);
                params.put("jarak", distance);
                params.put("waktu", duration);
                params.put("status", "0");

                logManager.logD(TAG, params.toString());
                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                R.integer.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
