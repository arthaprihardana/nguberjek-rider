package com.driver.nguberjek.model;

import java.io.Serializable;

/**
 * Created by masihsayang on 9/3/2016.
 */
public class OrderListModel implements Serializable{
    public String nama;
    public String jarak;
    public String harga;
    public String lokasi_awal;
    public String lokasi_akhir;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getLokasi_awal() {
        return lokasi_awal;
    }

    public void setLokasi_awal(String lokasi_awal) {
        this.lokasi_awal = lokasi_awal;
    }

    public String getLokasi_akhir() {
        return lokasi_akhir;
    }

    public void setLokasi_akhir(String lokasi_akhir) {
        this.lokasi_akhir = lokasi_akhir;
    }
}
