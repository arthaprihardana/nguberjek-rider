package com.driver.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sragen on 9/17/2016.
 */
public class SelfieOrderModel implements Serializable {
    public String created_at;
    public String status;
    public String waktu;
    public String jarak;
    public String harga;
    /*public String riderId;*/
    public String namaKonsumen;
    public String noTelpKonsumen;
    public String tanggal;
    public String orderId;
    public String _id;
    public String lokasiAwal;
    public String latitudeLA;
    public String longitudeLA;
    public String tujuan;
    public String latitudeLT;
    public String longitudeLT;

    public SelfieOrderModel(JSONObject data) throws JSONException {
        created_at = data.getString("created_at");
        status = data.getString("status");
        waktu = data.getString("waktu");
        jarak = data.getString("jarak");
        harga = data.getString("harga");
        /*riderId = data.getString("riderId");*/
        namaKonsumen = data.getString("namaKonsumen");
        noTelpKonsumen = data.getString("noTelpKonsumen");
        tanggal = data.getString("tanggal");
        orderId = data.getString("orderId");
        _id = data.getString("_id");
        lokasiAwal = data.getString("lokasiAwal");
        latitudeLA = data.getString("latitudeLA");
        longitudeLA = data.getString("longitudeLA");
        tujuan = data.getString("tujuan");
        latitudeLT = data.getString("latitudeLT");
        longitudeLT = data.getString("longitudeLT");
    }
}
