package com.driver.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Codelabs on 19/08/2016.
 */
public class StatusTopUpModel implements Serializable {
    public String _id;
    public String paymentCode;
    public String transactionStatus;
    public String expiredTime;
    public String transactionTime;
    public String paymentType;
    public String grossAmount;
    public String orderId;
    public String transactionID;
    public JSONObject rider;
    public String riderId;
    public String username;
    public String lokasi;
    public String email;
    public String nama;

    public StatusTopUpModel(JSONObject topUpList) throws JSONException {
        _id = topUpList.getString("_id");
        paymentCode = topUpList.getString("paymentCode");
        transactionStatus = topUpList.getString("transactionStatus");
        expiredTime = topUpList.getString("expiredTime");
        transactionTime = topUpList.getString("transactionTime");
        paymentType = topUpList.getString("paymentType");
        grossAmount = topUpList.getString("grossAmount");
        orderId = topUpList.getString("orderId");
        transactionID = topUpList.getString("transactionID");
        rider = topUpList.getJSONObject("riderId");
        riderId = rider.getString("_id");
        username = rider.getString("username");
        lokasi = rider.getString("lokasi");
        email = rider.getString("email");
        nama = rider.getString("nama");
    }

    /**
    String kode_pembayaran;
    String nominal;
    String status;

    public String getKode_pembayaran() {
        return kode_pembayaran;
    }

    public void setKode_pembayaran(String kode_pembayaran) {
        this.kode_pembayaran = kode_pembayaran;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
     */

}
