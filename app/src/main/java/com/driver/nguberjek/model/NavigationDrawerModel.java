package com.driver.nguberjek.model;

import java.io.Serializable;

/**
 * Created by Codelabs on 13/08/2016.
 */
public class NavigationDrawerModel implements Serializable {
    int menu;

    public int getMenu() {
        return menu;
    }

    public void setMenu(int menu) {
        this.menu = menu;
    }
}