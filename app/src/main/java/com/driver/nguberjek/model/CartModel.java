package com.driver.nguberjek.model;

import android.view.Menu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adikurniawan on 03/12/17.
 */

public class CartModel implements Serializable {
    // order //
    public String orderId="";
    public int subTotal=0;
    public int biayaAntar=0;
    public int total=0;
    public String alamatPengantaran="";
    public double orderLatitdue=0;
    public double orderLongitude=0;
    public String jarak="";

    // penumpang //
    public String boncengerId="";
    public String boncengerName="";
    public String boncengerPhone="";


    // resto //
    public String restonId="";
    public String restonName="";
    public String restoAddress="";
    public String restoPhone="";
    public double restoLatitude=0;
    public double restoLongitude=0;

    // cart menu //
    public ArrayList<MenuModel> menuList=new ArrayList<MenuModel>();







    public CartModel(JSONObject data)throws JSONException{

        this.orderId=data.getString("orderId");
        this.subTotal=data.getInt("perkiraanHarga");
        this.biayaAntar=data.getInt("biayaAntar");
        this.total=data.getInt("totalHarga");
        this.alamatPengantaran=data.getString("alamatPengantaran");
        JSONArray kordinatPengantaran=data.getJSONArray("koordinatPengantaran");
        this.orderLatitdue= Double.parseDouble(kordinatPengantaran.get(1).toString());
        this.orderLongitude= Double.parseDouble(kordinatPengantaran.get(0).toString());
        this.jarak=data.getString("jarak");


        JSONObject boncenger=data.getJSONObject("boncengerId");
        this.boncengerId=boncenger.getString("_id");
        this.boncengerName=boncenger.getString("nama");
        this.boncengerPhone=boncenger.getString("noTelp");


        JSONObject resto=data.getJSONObject("namaResto");
        this.restonId=resto.getString("_id");
        this.restonName=resto.getString("namaResto");
        this.restoAddress=resto.getString("alamat");
        this.restoPhone=resto.getString("noTelp");
        JSONArray kordinatResto=resto.getJSONArray("koordinat");
        this.restoLatitude=Double.parseDouble(kordinatResto.get(1).toString());
        this.restoLongitude=Double.parseDouble(kordinatResto.get(0).toString());


        JSONArray menu=data.getJSONArray("namaMenu");
        for(int i=0;i<menu.length();i++){
            JSONObject menuObj=menu.getJSONObject(i);
            JSONObject menuId=menuObj.getJSONObject("menuId");
            int qty=menuObj.getInt("jmlPorsi");
            String note=menuObj.getString("catatan");
            menuList.add(new MenuModel(menuId,qty,note));
        }


    }




}
