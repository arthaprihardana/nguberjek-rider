package com.driver.nguberjek.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by masihsayang on 9/3/2016.
 */
public class OrderDetailModel implements Serializable {
    public String _id;
    public String updated_at;
    public String created_at;
    public String kategori;
    public String status;
    public String waktu;
    public String jarak;
    public String harga;

    public String nama="";
    public String noTelp="";

    public String tanggal;
    public String orderId;
    public String show;
    public String setasfavorit;
    public String lokasiAwal;
    public String latitudeLA;
    public String longitudeLA;
    public String lokasiTujuan;
    public String latitudeLT;
    public String longitudeLT;

    public OrderDetailModel(JSONObject data) throws JSONException {
        _id = data.getString("_id");
        if(!data.isNull("updated_at"))updated_at = data.getString("updated_at");
        if(!data.isNull("created_at"))created_at = data.getString("created_at");
        if(!data.isNull("kategori"))kategori = data.getString("kategori");
        status = data.getString("status");
        waktu = data.getString("waktu");
        jarak = data.getString("jarak");

        if(!data.isNull("harga"))harga = data.getString("harga");

        orderId = data.getString("orderId");
        if(!data.isNull("show"))show = data.getString("show");
        if(!data.isNull("setasfavorit"))setasfavorit = data.getString("setasfavorit");
        lokasiAwal = data.getString("lokasiAwal");
        latitudeLA = data.getString("latitudeLA");
        longitudeLA = data.getString("longitudeLA");
        lokasiTujuan = data.getString("lokasiTujuan");
        latitudeLT = data.getString("latitudeLT");
        longitudeLT = data.getString("longitudeLT");

        if(!data.isNull("boncenger")) {
            JSONObject boncengerId = data.getJSONObject("boncenger");
            nama = boncengerId.getString("nama");
            noTelp = boncengerId.getString("noTelp");
        }

    }

    public OrderDetailModel(JSONObject data, boolean isFood) throws JSONException {
        _id = data.getString("_id");
        if(!data.isNull("updated_at"))updated_at = data.getString("updated_at");
        if(!data.isNull("created_at"))created_at = data.getString("created_at");
        if(!data.isNull("kategori"))kategori = data.getString("kategori");
        status = data.getString("status");
        waktu = data.getString("waktu");
        jarak = data.getString("jarak");

        if(!data.isNull("totalHarga"))harga = data.getString("totalHarga");

        orderId = "food_"+data.getString("orderId");
        if(!data.isNull("show"))show = data.getString("show");
        if(!data.isNull("setasfavorit"))setasfavorit = data.getString("setasfavorit");

        JSONObject dataResto=data.getJSONObject("resto");
        lokasiAwal = dataResto.getString("alamat");
        JSONArray cor_=dataResto.getJSONArray("koordinat");
        latitudeLA = cor_.get(1).toString();
        longitudeLA = cor_.get(0).toString();


        lokasiTujuan = data.getString("alamatPengantaran");
        JSONArray cor=data.getJSONArray("koordinatPengantaran");
        this.latitudeLT=cor.get(1).toString();
        this.longitudeLT=cor.get(0).toString();

        if(!data.isNull("boncenger")) {
            JSONObject boncengerId = data.getJSONObject("boncenger");
            nama = boncengerId.getString("nama");
            noTelp = boncengerId.getString("noTelp");
        }
    }
}
