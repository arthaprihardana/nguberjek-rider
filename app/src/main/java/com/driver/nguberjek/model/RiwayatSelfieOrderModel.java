package com.driver.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sragen on 9/27/2016.
 */
public class RiwayatSelfieOrderModel implements Serializable{
    public String tanggal;
    public String lokasi_awal;
    public String lokasi_akhir;
    public String jarak;
    public String harga;

    public RiwayatSelfieOrderModel(JSONObject riwayat)throws JSONException {
        tanggal=riwayat.getString("tanggal");
        jarak=riwayat.getString("jarak");
        harga=riwayat.getString("harga");
        lokasi_awal=riwayat.getString("lokasiAwal");
        lokasi_akhir=riwayat.getString("tujuan");
    }
}
