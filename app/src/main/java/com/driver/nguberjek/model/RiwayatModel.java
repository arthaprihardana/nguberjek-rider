package com.driver.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Nur If Alan on 16/08/2016.
 */
public class RiwayatModel implements Serializable {
    public String _id;
    public String orderId;
    public String kategori;
    public String tanggal;
    public String lokasiAwal;
    public String latitudeLA;
    public String longitudeLA;
    public String lokasiTujuan;
    public String latitudeLT;
    public String longitudeLT;
    public String jarak;
    public String waktu;
    public String harga;
    public String status;

    public JSONObject driver;
    public String nama;
    public String noTelp;
    public  String email;
    public String photo;
    public String jenisKelamin;
    public String typeKendaraan;
    public String merkKendaraan;
    public String noPolisi;
    public String lokasi;

    public JSONObject boncenger;
    public String namaBoncenger;
    public String noTelpBocenger;
    public String emailBocenger;
    public String jenisKelaminBocenger;
    public String photoBocenger;

    public RiwayatModel(JSONObject riwayat)throws JSONException{
        _id=riwayat.getString("_id");
        orderId=riwayat.getString("orderId");
        kategori=riwayat.getString("kategori");
        tanggal=riwayat.getString("tanggal");
        lokasiAwal=riwayat.getString("lokasiAwal");
        latitudeLA=riwayat.getString("latitudeLA");
        longitudeLA=riwayat.getString("longitudeLA");
        lokasiTujuan=riwayat.getString("lokasiTujuan");
        latitudeLT=riwayat.getString("latitudeLT");
        longitudeLT=riwayat.getString("longitudeLT");
        jarak=riwayat.getString("jarak");
        waktu=riwayat.getString("waktu");
        harga=riwayat.getString("harga");
        status=riwayat.getString("status");

        driver=riwayat.getJSONObject("driver");
        nama=driver.getString("nama");
        noTelp=driver.getString("noTelp");
        email=driver.getString("email");
        photo=driver.getString("photo");
        jenisKelamin=driver.getString("jenisKelamin");
        typeKendaraan=driver.getString("typeKendaraan");
        merkKendaraan=driver.getString("merkKendaraan");
        noPolisi=driver.getString("noPolisi");
        lokasi=driver.getString("lokasi");

        boncenger=riwayat.getJSONObject("boncenger");
        namaBoncenger=boncenger.getString("nama");
        noTelpBocenger=boncenger.getString("noTelp");
        emailBocenger=boncenger.getString("email");
        jenisKelaminBocenger=boncenger.getString("jenisKelamin");
        photoBocenger=boncenger.getString("photo");
    }
}
