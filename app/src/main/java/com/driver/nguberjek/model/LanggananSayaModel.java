package com.driver.nguberjek.model;

import java.io.Serializable;

/**
 * Created by Codelabs on 16/08/2016.
 */
public class LanggananSayaModel implements Serializable{
    String nama_pelanggan;
    String no_pelanggan;

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getNo_pelanggan() {
        return no_pelanggan;
    }

    public void setNo_pelanggan(String no_pelanggan) {
        this.no_pelanggan = no_pelanggan;
    }
}
