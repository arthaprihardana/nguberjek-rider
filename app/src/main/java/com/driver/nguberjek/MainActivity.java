package com.driver.nguberjek;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.driver.nguberjek.utilities.ActivityManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnMasuk;
    TextView btnDaftar, btnAplikasiPenumpang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setContent();

        btnMasuk.setOnClickListener(this);
        btnDaftar.setOnClickListener(this);
        btnAplikasiPenumpang.setOnClickListener(this);
    }

    public void setContent(){
        btnMasuk = (Button) findViewById(R.id.btn_masuk);
        btnDaftar = (TextView) findViewById(R.id.btn_daftar);
        btnAplikasiPenumpang = (TextView) findViewById(R.id.btn_aplikasiPenumpang);
    }

    @Override
    public void onClick(View v) {
        if(v == btnMasuk){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }else if(v == btnDaftar){
            Uri uri = Uri.parse("http://nguberjek.com/gabung"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }else if(v == btnAplikasiPenumpang){
            Uri uri = Uri.parse("market://details?id=com.boncenger.nguberjek");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                v.getContext().startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                v.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=com.nguberjek.boncenger")));
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

        ActivityManager.getInstance().finishAll();
    }
}