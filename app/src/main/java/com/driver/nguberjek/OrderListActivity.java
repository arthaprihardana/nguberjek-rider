package com.driver.nguberjek;

import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.driver.nguberjek.adapter.OrderListAdapter;
import com.driver.nguberjek.model.OrderListModel;
import com.driver.nguberjek.utilities.BaseActivity;

import java.util.ArrayList;

public class OrderListActivity extends BaseActivity {
    private final String TAG = OrderListActivity.class.getSimpleName();

    ArrayList<OrderListModel> orderListModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorlayout);

        setActionBar();
        takeOrderListData();
        getData();
    }

    public void setActionBar() {
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
        tvTittle.setText("Order Masuk");

        getSupportActionBar().setCustomView(mCustomView, params);
    }

    public void takeOrderListData(){
        OrderListModel orderListModel = new OrderListModel();
        orderListModel.setNama("Ayu Budi");
        orderListModel.setJarak("10");
        orderListModel.setHarga("30000");
        orderListModel.setLokasi_awal("Pondok Mitra Lestari");
        orderListModel.setLokasi_akhir("Stasiun Bekasi");

        orderListModels.add(orderListModel);

        orderListModel = new OrderListModel();
        orderListModel.setNama("Iman Arifian");
        orderListModel.setJarak("10");
        orderListModel.setHarga("30000");
        orderListModel.setLokasi_awal("Pondok Mitra Lestari");
        orderListModel.setLokasi_akhir("Stasiun Bekasi");

        orderListModels.add(orderListModel);
    }

    public void getData(){
        RecyclerView rvOrderList = (RecyclerView) findViewById(R.id.rv_orderListData);
        RelativeLayout rlNoRiwayatData = (RelativeLayout) findViewById(R.id.rl_noRiwayatData);

        rvOrderList.setLayoutManager(new LinearLayoutManager(this));

        if(orderListModels.size() != 0){
            rvOrderList.setVisibility(View.VISIBLE);
            rlNoRiwayatData.setVisibility(View.GONE);

            OrderListAdapter orderListAdapter = new OrderListAdapter(OrderListActivity.this, orderListModels);
            rvOrderList.setAdapter(orderListAdapter);
        }else{
            rvOrderList.setVisibility(View.GONE);
            rlNoRiwayatData.setVisibility(View.VISIBLE);
        }
    }
}
