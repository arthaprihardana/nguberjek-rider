package com.driver.nguberjek.pushnotification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.driver.nguberjek.ChatActivity;
import com.driver.nguberjek.NewOrder_Activity;
import com.google.android.gms.gcm.GcmListenerService;
import com.driver.nguberjek.HomeActivity;
import com.driver.nguberjek.InformasiDialogActivity;
import com.driver.nguberjek.NewOrderActivity;
import com.driver.nguberjek.OrderViewActivity;
import com.driver.nguberjek.R;
import com.driver.nguberjek.app.AppConstans;
import com.driver.nguberjek.utilities.ActivityManager;
import com.driver.nguberjek.utilities.SessionManager;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Codelabs on 29/01/2016.
 */
public class MyGcmListenerService extends GcmListenerService {
    private static final String TAG = "MyGcmListenerService";

    public SessionManager sessionManager;
    public HashMap<String, String> user;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String status = data.getString("status");
        String title = data.getString("title");
        String message = data.getString("message");
        String orderId = data.getString("orderId");

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();

        Log.d("from : ", from.toString());
        Log.d("pushnotif : ", data.toString());

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        if(status.equals("0")) {

        }

        if (status.equals("0") || status.equals("2") || status.equals("8") || status.equals("11") || status.equals("5") || status.equals("6") || ChatActivity.isActive) {
            goToActivity(orderId, status, title, message);
        } else {
            playSound();
            sendNotification(title, status, message, orderId);
        }
    }
    // [END receive_message]

    private void goToActivity(String orderId, String status, String tittle, String message) {
        //Toast.makeText(getApplicationContext(), "Go Activity", Toast.LENGTH_LONG).show();

        Intent i;
        Bundle b = new Bundle();
        playSound();

        if (status.equals("0")) {
            i = new Intent(this, NewOrder_Activity.class);
            b.putString("orderId", orderId);
            b.putString("pushNotif", "0");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtras(b);
            startActivity(i);
        } else if (status.equals("2")) {
            AppConstans.orderID = "";

            i = new Intent(this, OrderViewActivity.class);
            b.putString("orderId", orderId);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtras(b);
            startActivity(i);
            ActivityManager.getInstance().finishAll();
        } else if (status.equals("6")) {
            AppConstans.orderID = "";

            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            b.putString("informasi", message);
            intent.putExtras(b);
            startActivity(intent);
            ActivityManager.getInstance().finishAll();
        } else if (status.equals("8")) {
            if (AppConstans.orderID.equals(orderId)) {
                i = new Intent(this, NewOrder_Activity.class);
                b.putString("orderId", orderId);
                b.putString("pushNotif", "8");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtras(b);
                startActivity(i);
            }
        } else if (status.equals("11")) {
            if (AppConstans.orderID.equals(orderId)) {
                i = new Intent(this, NewOrder_Activity.class);
                b.putString("orderId", orderId);
                b.putString("pushNotif", "11");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtras(b);
                startActivity(i);
            }
        } else if (status.equals("14")) {
            i = new Intent(this, ChatActivity.class);
            b.putString("orderId", orderId);
            b.putString("pushNotif", "14");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtras(b);
            startActivity(i);
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void sendNotification(String title, String status, String message, String orderId) {

        Intent intent = new Intent();
        if (status.equals("10")) {
            intent = new Intent(this, InformasiDialogActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("informasi", message);
            intent.putExtras(b);
        } else if (status.equals("14")) {
            intent = new Intent(this, ChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("orderId", orderId);
            b.putString("message", message);
            intent.putExtras(b);
        } else if (status.equals("12")) {
//            if (AppConstans.orderID.equals(orderId)) {
//            intent = new Intent(this, TopUpActivity.class);
            intent = new Intent(this, InformasiDialogActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                b.putString("orderId", orderId);
            Bundle b = new Bundle();
//            b.putString("pushNotif", "12");
            b.putString("informasi", message);
            intent.putExtras(b);
//            startActivity(intent);
//            }
        }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.icon_rider)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder
                    .setFullScreenIntent(fullScreenPendingIntent, true);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public void playSound() {
        Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        MediaPlayer mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(this, defaultRingtoneUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
